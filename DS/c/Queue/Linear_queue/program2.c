// Implimenting Queue using Link List

#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

    int data;
    struct Node *next;
}Node;

Node *front = NULL;
Node *rear = NULL;
int flag = 0;

Node* createNode(){

    Node* newNode = (Node*)malloc(sizeof(Node));

    if(newNode == NULL){
        printf("Memory FULL(Heap)\n");
        exit(0);
    }else{
        printf("Enter Data:: ");
        scanf("%d",&newNode->data);

        newNode->next = NULL;

        return newNode;
    }
}
int enqueue(){

    Node *newNode = createNode();
    if(front == NULL){
        front = newNode;
        rear = newNode;
    }else{
        rear->next = newNode;
        rear = newNode;
    }
    return 0;
}
int dequeue(){ 

    if(front == NULL){
        flag = 0;
        return -1;
    }else{
        flag = 1;
        if(front->next == NULL){
            int val = front->data;
            free(front);
            front = NULL;
            rear = NULL;
            return val;
        }else{
            Node *temp = front;
            int val = temp->data;
            front = front->next;
            free(temp);
            return val;
        }
    }
}
int frontt(){

    if(front == NULL){
        flag = 0;
        return -1;
    }else{
        flag = 1;
        return front->data;
    }
}
int PrintQueue(){

    if(front == NULL){
        return -1;
    }else{
        Node *temp = front;
        while(temp != NULL){
            printf("%d ",temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}
void main(){

    char choise;
    do{
        printf("1.enqueue\n");
        printf("2.dequeue\n");
        printf("3.frontt\n");
        printf("4.PrintQueue\n");

        int ch;
        printf("Enter Choise:: ");
        scanf("%d",&ch);

        switch(ch){

            case 1:
                enqueue();
                break;
            case 2:
                {
                    int ret = dequeue();
                    if(flag == 1){
                        printf("%d is dequeued\n",ret);              
                    }else{
                        printf("Queue is empty\n");
                    }
                }
                break;
            case 3:
                {
                    int ret = frontt();
                    if(flag == 1){
                        printf("%d is front element\n1",ret);
                    }else{
                        printf("Queue is empty\n");
                    }
                }
                break;
            case 4:
                {
                    int ret = PrintQueue();
                    if(ret == -1){
                        printf("Queue is empty\n");
                    }
                }
                break;
            default:
                printf("Wrong Choise\n");
        }
        getchar();
        printf("Do you wnat to continue [Y/y]:: ");
        scanf("%c",&choise);
    }while(choise == 'Y' || choise == 'y');
}