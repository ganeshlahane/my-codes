// Implimenting Circular Queue using Link List

#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

    int data;
    struct Node *next;
}Node;

Node *front = NULL;
Node *rear = NULL;
int flag = 0;
int size = 0;
int cnt = 0;

Node* createNode(){

    Node* newNode = (Node*)malloc(sizeof(Node));

    if(newNode == NULL){
        printf("Memory FULL(Heap)\n");
        exit(0);
    }else{
        printf("Enter Data:: ");
        scanf("%d",&newNode->data);

        newNode->next = NULL;

        return newNode;
    }
}
int nodeCount(){

    Node *temp = front;
    int count = 0;
    while(temp->next != front){
        count++;
        temp = temp->next;
    }
    count++;
    return count;
}
int enqueue(){
   
    Node *newNode = createNode();
    cnt++;
    if(front == NULL){
        front = newNode;
        rear = newNode;
        front->next = newNode;
    }else{
        rear->next = newNode;
        newNode->next = front;
        rear = newNode;
    }
}
int dequeue(){

    if(front == NULL){
        flag = 0;
        return -1;
    }else{
        flag = 1;
        if(front->next == front){
            int val = front->data;
            free(front);
            front = NULL;
            rear = NULL;
            return val;
        }else{
            Node *temp = front;
            int val = front->data;
            front = front->next;
            rear->next = front;
            free(temp);
            return val;
        }
    }
}
int frontt(){

    if(front == NULL){
        flag = 0;
        return -1;
    }else{
        flag = 1;
        return front->data;
    }
}
int PrintQueue(){

    if(front == NULL){
        flag = 0;
        return -1;
    }else{
        Node *temp = front;
        while(temp->next != front){
            printf("%d ",temp->data);
            temp = temp->next;
        }
        printf("%d",temp->data);
    }
    printf("\n");
}
void main(){

    printf("Enter Queue Size:: ");
    scanf("%d",&size);

    char choise;
    do{
        printf("1.enqueue\n");
        printf("2.dequeue\n");
        printf("3.frontt\n");
        printf("4.PrintQueue\n");

        int ch;
        printf("Enter Choise:: ");
        scanf("%d",&ch);

        switch(ch){

            case 1:
                {
                    if(size > cnt){
                        enqueue();
                    }else{
                        printf("Queue is Full\n");
                    }
                }
                break;
            case 2:
                {
                    int ret = dequeue();
                    if(flag == 1){
                        printf("%d is dequeued\n",ret);              
                    }else{
                        printf("Queue is empty\n");
                    }
                }
                break;
            case 3:
                {
                    int ret = frontt();
                    if(flag == 1){
                        printf("%d is front element\n",ret);
                    }else{
                        printf("Queue is empty\n");
                    }
                }
                break;
            case 4:
                {
                    int ret = PrintQueue();
                    if(ret == -1){
                        printf("Queue is empty\n");
                    }
                }
                break;
            default:
                printf("Wrong Choise\n");
        }
        getchar();
        printf("Do you wnat to continue [Y/y]:: ");
        scanf("%c",&choise);
    }while(choise == 'Y' || choise == 'y');
}
