// Circular Queue using array
#include <stdio.h>

int front = -1;
int rear = -1;
int size = 0;
int flag = 0;

int enqueue(int arr[]){

    if((rear == size-1 && front == 0) || rear == front-1){
        return -1;
    }else{
        if(front == -1){
            front++;
        }else{
            if(rear == size-1 && front != 0){
                rear = -1;
            }
        }
        rear++;
        printf("Enter Data:: ");
        scanf("%d",&arr[rear]);
        return 0;
    }
}
int dequeue(int arr[]){

    if(front == -1){
        flag = 0;
        return -1;
    }else{
        flag = 1;
        int val = arr[front];
        if(rear == front){
            rear = -1;
            front = -1;
        }else{
            if(front == size-1){
                front = -1;
            }
            front++;
        }
        return val;
    }
}
int frontt(int arr[]){

    if(front == -1){
        flag = 0;
        return -1;
    }else{
        flag = 1;
        return arr[front];
    }
}
int PrintQueue(int arr[]){

    if(front == -1){
        flag = 0;
        return -1;
    }else{
        flag = 1;
        if(front <= rear){
        for(int i=front; i<=rear; i++){
            printf("%d ",arr[i]);
        }
        }else{
            for(int i=front; i<=size-1; i++){
                printf("%d ",arr[i]);
            }
            for(int i=0; i<=rear; i++){
                printf("%d ",arr[i]);
            }
        }
    }
    printf("\n");
}
void main(){

    printf("Enter array size:: ");
    scanf("%d",&size);

    int arr[size];
    char choise;
    do{
        printf("1.Enqueue\n");
        printf("2.Dequeue\n");
        printf("3.Front\n");
        printf("4.PrintQueue\n");

        int ch;
        printf("Enter Choise:: ");
        scanf("%d",&ch);

        switch(ch){

            case 1:
                {
                    int ret = enqueue(arr);
                    if(ret == -1){
                    printf("Queue overflow\n");
                    }
                }
                break;
            case 2:
                {
                    int ret = dequeue(arr);
                    if(flag == 1){
                        printf("%d is dequeued\n",ret);
                    }else{
                        printf("Queue Underflow\n");
                    }
                }
                break;
            case 3:
                {
                    int ret = frontt(arr);
                    if(flag == 1){
                        printf("%d is fornt element\n",ret);
                    }else{
                        printf("Queue is Empty\n");
                    }
                }
                break;
            case 4:
                {
                    int ret = PrintQueue(arr);
                    if(flag == 0){
                        printf("Queue is Empty\n");
                    }
                }
                break;
            default:
                printf("Wrong Choise\n");
                break;
        }
        getchar();
        printf("Do you want to continue [Y/y]:: ");
        scanf("%c",&choise);
    }while(choise == 'Y' || choise == 'y');
}