// Search a given number from the array.
#include <stdio.h>

int SearchNum(int arr[],int size,int key){

    int start=0,end=size-1,mid;

    while(start<=end){

        mid = (start+end)/2;

        if(arr[mid]==key){
            return mid;
        }
        if(arr[mid]<key){
            start = mid+1;
        }else{
            end = mid-1;
        }
    }
    return -1;
}
void main(){

    int size;
    printf("Enter Array Size: ");
    scanf("%d",&size);

    int arr[size];
    printf("Enter Array Element:\n");
     
    for(int i=0; i<size; i++){

        scanf("%d",&arr[i]);
    }
    printf("=================================\n");
    printf("Our array is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");

    int key;
    printf("Enter key element you want to search: ");
    scanf("%d",&key);
    int ret = SearchNum(arr,size,key);

    if(ret != -1){
        printf("Key element is present at %d index\n",ret);
    }else{
        printf("Element is not present\n");
    }

}