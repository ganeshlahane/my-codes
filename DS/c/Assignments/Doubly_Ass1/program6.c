/*
	Program 6.
	Weite a program that accepts a doubly linked list from the user.
	Take a number from the user and print the data of the length of that
	number. Length of kanha=5
	Input: linked list: |Shashi |-> | Ashish|-> |Kanha |-> | Rahul |-> | Badhe |
	Input: Enter Length 5
	Output:
	Kanha
	Rahul
	Badhe
*/
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	struct Node *prev;
	char str[20];
	struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){

	Node *newNode =(Node*)malloc(sizeof(Node));

	getchar();
	printf("Enter Name:: ");
	gets(newNode->str);
	newNode->next = NULL;
	return newNode;
}

void addNode(){

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

void Operation(){

	int len,i=0;
	printf("Enter which length str you want to print:: ");
	scanf("%d",&len);

	Node *temp = head;
	while(temp != NULL){

		while(temp->str[i] != '\0'){
			i++;
		}
		if(i == len){
			printf("%s\n",temp->str);
		}
		temp = temp->next;
	}
}

void printLL(){

	Node *temp = head;
	if(head == NULL){
		printf("LL is Empty\n");
	}else{
		while(temp->next !=  NULL){
			printf("|%s|<-->",temp->str);
			temp = temp->next;
		}
		printf("|%s|",temp->str);
		printf("\n");
	}
}

void main(){

	int noOfNodes;
	printf("Enter how many ndoes you want to create?:: ");
	scanf("%d",&noOfNodes);

	char ch;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.Operation\n");

		int num;
		printf("Enter Choise:: ");
		scanf("%d",&num);

		switch(num){

		case 1:
			{
				for(int i=1; i<=noOfNodes; i++){
					addNode();
				}
			}
			break;
		case 2:
			printLL();
			break;
		case 3:
			Operation();
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue? [Y/y] ");
		scanf("%c",&ch);
	}while(ch == 'Y' || ch == 'y');
}
