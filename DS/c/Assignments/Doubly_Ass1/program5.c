/*
	Program 5.
	Write a program that searches all the Palindrome data elements from a
	doubly linked list. And Print the position of palindrome data.
	Input: linked list: |12|->|121|->|30|->|252|->|35|->|151|->|70|
	Output:
	Palindrome found at 2
	Palindrome found at 4
	Palindrome found at 6
*/
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	struct Node *prev;
	int data;
	struct Node *next;
}Node;
Node *head = NULL;

Node* createNode(){

	Node *newNode =(Node*)malloc(sizeof(Node));

	newNode->prev = NULL;

	printf("Enter Data:: ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

int Opperation(int num){

	int rem = 0,rev = 0;
	while(num != 0){
		rem = num%10;
		rev = rev*10+rem;
		num = num/10;
	}
	return rev;
}

void FindPelindrome(){

	Node *temp = head;
	int count = 0;

	while(temp != NULL){

		count++;
		if(temp->data == Opperation(temp->data)){
			printf("Pelindrome found at %d\n",count);
		} 
		temp = temp->next;
	}
}

void printLL(){

	Node *temp = head;
	if(head == NULL){
		printf("LL is Empty\n");
	}else{
		while(temp->next !=  NULL){
			printf("|%d|<-->",temp->data);
			temp = temp->next;
		}
		printf("|%d|",temp->data);
		printf("\n");
	}
}

void main(){

	int noOfNodes;
	printf("Enter how many ndoes you want to create?:: ");
	scanf("%d",&noOfNodes);

	char ch;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.Occurrence\n");

		int num;
		printf("Enter Choise:: ");
		scanf("%d",&num);

		switch(num){

		case 1:
			{
				for(int i=1; i<=noOfNodes; i++){
					addNode();
				}
			}
			break;
		case 2:
			printLL();
			break;
		case 3:
			FindPelindrome();
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue? [Y/y] ");
		scanf("%c",&ch);
	}while(ch == 'Y' || ch == 'y');
}