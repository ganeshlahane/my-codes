/*
	Program 4.
	Write a program that adds the digits of a data element from a doubly linked
	list and changes the data. (sum of data element digits)
	Input linked list : |11|->|12|->|13|->|141|->|2|->|158|
	Output linked list : |2|->|3|->|4|->|6|->|2|->|14|
*/
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	struct Node *prev;
	int data;
	struct Node *next;
}Node;
Node *head = NULL;

Node* createNode(){

	Node *newNode =(Node*)malloc(sizeof(Node));

	newNode->prev = NULL;

	printf("Enter Data:: ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

int sumOfDigits(int num){

	int rem=0,sum=0;
	while(num != 0){
		rem = num%10;
		sum = sum+rem;
		num = num/10;
	}
	return sum;
}

void Operation(){

	Node *temp = head;

	while(temp != NULL){

		temp->data  = sumOfDigits(temp->data);
		temp = temp->next;
	}
}
void printLL(){

	Node *temp = head;
	if(head == NULL){
		printf("LL is Empty\n");
	}else{
		while(temp->next !=  NULL){
			printf("|%d|<-->",temp->data);
			temp = temp->next;
		}
		printf("|%d|",temp->data);
		printf("\n");
	}
}

void main(){

	int noOfNodes;
	printf("Enter how many ndoes you want to create?:: ");
	scanf("%d",&noOfNodes);

	char ch;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.Operation\n");

		int num;
		printf("Enter Choise:: ");
		scanf("%d",&num);

		switch(num){

		case 1:
			{
				for(int i=1; i<=noOfNodes; i++){
					addNode();
				}
			}
			break;
		case 2:
			printLL();
			break;
		case 3:
			Operation();
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue? [Y/y] ");
		scanf("%c",&ch);
	}while(ch == 'Y' || ch == 'y');
}