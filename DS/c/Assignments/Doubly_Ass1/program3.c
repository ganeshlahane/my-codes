/*
	Program 3.
	Write a program that searches the occurrence of a particular element from
	a doubly linked list.
	Input linked list: |10|->|20|->|30|->|40|->|50|->|30|->|70|
	Input Enter element: 30
	Output: 2 times
*/
#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	struct Node *prev;
	int data;
	struct Node *next;
}Node;
Node *head = NULL;

Node* createNode(){

	Node *newNode =(Node*)malloc(sizeof(Node));

	newNode->prev = NULL;

	printf("Enter Data:: ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

int numCount(int num){

	Node *temp = head;
	int count = 0;
	while(temp != NULL){
		if(temp->data == num){
			count++;
		}
		temp = temp->next;
	}
	return count;
}
void Occurrence(int num){

	int count = numCount(num);
	if(count == 0){
		printf("Number is not present in Linked List\n");
	}else{
		printf("%d times",count);
	}
	printf("\n");
}

void printLL(){

	Node *temp = head;
	if(head == NULL){
		printf("LL is Empty\n");
	}else{
		while(temp->next !=  NULL){
			printf("|%d|<-->",temp->data);
			temp = temp->next;
		}
		printf("|%d|",temp->data);
		printf("\n");
	}
}

void main(){

	int noOfNodes;
	printf("Enter how many ndoes you want to create?:: ");
	scanf("%d",&noOfNodes);

	char ch;
	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.Occurrence\n");

		int num;
		printf("Enter Choise:: ");
		scanf("%d",&num);

		switch(num){

		case 1:
			{
				for(int i=1; i<=noOfNodes; i++){
					addNode();
				}
			}
			break;
		case 2:
			printLL();
			break;
		case 3:
			{
				int num;
				printf("Enter Number for search:: ");
				scanf("%d",&num);
				Occurrence(num);
			}
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue? [Y/y] ");
		scanf("%c",&ch);
	}while(ch == 'Y' || ch == 'y');
}