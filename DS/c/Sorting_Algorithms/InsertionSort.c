#include <stdio.h>
void InsertionSort(int arr[],int size){

    for(int i=1; i<size; i++){
        int val = arr[i];
        int j = i-1;

        for( ; j>=0 && arr[j]>val; j--){

            arr[j+1] = arr[j];
        }
        arr[j+1] = val;
    }
}
void main(){
    int size;
    printf("Enter Array Size: ");
    scanf("%d",&size);

    int arr[size];
    printf("Enter Array Element:\n");
    
    for(int i=0; i<size; i++){

        scanf("%d",&arr[i]);
    }
    printf("=================================\n");
    printf("Our array is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");

    InsertionSort(arr,size);

    printf("=================================\n");
    printf("Our array sorted is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");
}