#include <stdio.h>
void SelectionSort(int arr[],int size){

    for(int i=0; i<size; i++){
        int minindex = i;
        for(int j=i+1; j<size; j++){
            if(arr[minindex]>arr[j]){
                minindex = j;
            }
        }
        int temp = arr[i];
        arr[i] = arr[minindex];
        arr[minindex] = temp;
    }
}
void main(){
    int size;
    printf("Enter Array Size: ");
    scanf("%d",&size);

    int arr[size];
    printf("Enter Array Element:\n");
    
    for(int i=0; i<size; i++){

        scanf("%d",&arr[i]);
    }
    printf("=================================\n");
    printf("Our array is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");

    SelectionSort(arr,size);

    printf("=================================\n");
    printf("Our array sorted is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");
}