// In this approach the last element is cosider as pivot element.

#include <stdio.h>

void swap(int *x,int *y){

	int temp = *x;
	*x = *y;
	*y = temp;
}
int partition(int arr[],int start,int end){

	int itr = start-1;
	for(int i=start; i<end; i++){

		if(arr[i]<arr[itr]){
			itr++;
			swap(&arr[i],&arr[itr]);
		}
	}
	swap(&arr[itr+1],&arr[end]);
	return itr+1;	
}
void QuickSort(int arr[],int start,int end){
	if(start<end){
		int pivot = partition(arr,start,end);

		QuickSort(arr,start,pivot-1);
		QuickSort(arr,pivot+1,end);
	}
}
void main(){
    int size;
    printf("Enter Array Size: ");
    scanf("%d",&size);

    int arr[size];
    printf("Enter Array Element:\n");
    
    for(int i=0; i<size; i++){

        scanf("%d",&arr[i]);
    }
    printf("=================================\n");
    printf("Our array is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");

    int start=0,end=size-1;

    QuickSort(arr,start,end);

    printf("=================================\n");
    printf("Our array sorted is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");
}