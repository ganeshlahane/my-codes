#include <stdio.h>
void BubbleSort(int arr[],int size){

    for(int i=0; i<size; i++){
        for(int j=0; j<size-i-1; j++){
            if(arr[j]>arr[j+1]){
                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }
}
void main(){
    int size;
    printf("Enter Array Size: ");
    scanf("%d",&size);

    int arr[size];
    printf("Enter Array Element:\n");
    
    for(int i=0; i<size; i++){

        scanf("%d",&arr[i]);
    }
    printf("=================================\n");
    printf("Our array is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");

    BubbleSort(arr,size);

      printf("=================================\n");
    printf("Our array sorted is::::== ");
    for(int i=0; i<size; i++){

        printf("|%d",arr[i]);
    }
    printf("|");
    printf("\n");
}