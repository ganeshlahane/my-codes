#include <stdio.h>
#include <string.h>

typedef struct Employee {

	int empId;
	char empName[20];
	float sal;
	struct Employee *next; 
}Emp;

void main(){

	Emp obj1,obj2,obj3;

	Emp *head = &obj1;

	head->empId = 10;
	strcpy(head->empName,"Ganesh");
	head->sal = 20.50;
	head->next = &obj2;

	head->next->empId = 20;
	strcpy(head->next->empName,"Karan");
	head->next->sal = 25.50;
	head->next->next = &obj3;

	head->next->next->empId = 30;
	strcpy(head->next->next->empName,"Mangesh");
	head->next->next->sal = 30.50;
	head->next->next->next = NULL;

	printf("%d\n",head->empId);
	puts(head->empName);
	printf("%f\n",head->sal);

	printf("%d\n",head->next->empId);
	puts(head->next->empName);
	printf("%f\n",head->next->sal);

	printf("%d\n",head->next->next->empId);
	puts(head->next->next->empName);
	printf("%f\n",head->next->next->sal);
}
