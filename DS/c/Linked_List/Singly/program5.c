#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct Movie {

	char Name[20];
	int tickcount;
	float rating;
	struct Movie *next;
}M;
void viewData(M *ptr){

	puts(ptr->Name);
	printf("%d\n",ptr->tickcount);
	printf("%f\n",ptr->rating);
}
void main(){

	M *m1 = (M*)malloc(sizeof(M));
	M *m2 = (M*)malloc(sizeof(M));
	M *m3 = (M*)malloc(sizeof(M));

	strcpy(m1->Name,"Kantara");
	m1->tickcount = 10;
	m1->rating = 9.5;
	m1->next = m2;

	strcpy(m2->Name,"Drishyam 2");
	m2->tickcount = 5;
	m2->rating = 8.5;
	m2->next = m3;

	strcpy(m3->Name,"Bhediya");
	m3->tickcount = 2;
	m3->rating = 7.5;
	m3->next = NULL;

	viewData(m1);
	viewData(m2);
	viewData(m3);
}