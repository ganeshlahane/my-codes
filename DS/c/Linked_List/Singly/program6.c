#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;
}Node;

void main(){

	Node *head = NULL; 

	Node *newNode = (Node*)malloc(sizeof(Node));
	newNode->data = 10;
	newNode->next = NULL;

	head = newNode; // connecting First Node

	newNode = (Node*)malloc(sizeof(Node));
	newNode->data = 20;
	newNode->next = NULL;

	head->next = newNode; // connecting Second Node

	newNode = (Node*)malloc(sizeof(Node));
	newNode->data = 30;
	newNode->next = NULL;

	head->next->next = newNode; // connecting Third Node

	Node *temp = head;

	while(temp != NULL){

		printf("%d|",temp->data);
		temp = temp->next;
	}
	printf("\n");
}
