#include <stdio.h>
#include <string.h>

typedef struct CricPlayer {

	char Name[20];
	int jerNo;
	float avg;
	struct CricPlayer *nextPlayer;
}CP;

void main(){

	CP P1,P2,P3;

	CP *head = &P1;

	strcpy(head->Name,"Shikhar Dhawan");
	head->jerNo = 42;
	head->avg = 42.5;
	head->nextPlayer = &P2;

	strcpy(head->nextPlayer->Name,"Shubhman Gill");
	head->nextPlayer->jerNo = 7;
	head->nextPlayer->avg = 45.7;
	head->nextPlayer->nextPlayer = &P3;

	strcpy(head->nextPlayer->nextPlayer->Name,"Shreyash Iyer");
	head->nextPlayer->nextPlayer->jerNo = 41;
	head->nextPlayer->nextPlayer->avg = 40.2;
	head->nextPlayer->nextPlayer->nextPlayer = NULL;

	puts(head->Name);
	printf("%d\n",head->jerNo);
	printf("%f\n",head->avg);

	puts(head->nextPlayer->Name);
	printf("%d\n",head->nextPlayer->jerNo);
	printf("%f\n",head->nextPlayer->avg);

	puts(head->nextPlayer->nextPlayer->Name);
	printf("%d\n",head->nextPlayer->nextPlayer->jerNo);
	printf("%f\n",head->nextPlayer->nextPlayer->avg);
}
