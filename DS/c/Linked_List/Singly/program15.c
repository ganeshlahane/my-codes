// Real time example

#include <stdio.h>
#include <stdlib.h>

typedef struct TeaFranchise {

	char fName[20];
	int noOfBranches;
	struct TeaFranchise *next;
}Tea;

Tea *head = NULL;

Tea* createNode(){

	Tea* newNode = (Tea*)malloc(sizeof(Tea));

	getchar();
	printf("Enter Franchise name:: ");
	char ch;
	int i=0;
	while((ch = getchar()) != '\n'){

		(*newNode).fName[i] = ch;
		i++;
	}
	printf("Enter no. of Branches:: ");
	scanf("%d",&newNode->noOfBranches);

	newNode->next = NULL;
	return newNode;
}
void addNode(){

	Tea *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		Tea *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void addFirst(){

	Tea *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head = newNode;
	}
}
void addLast(){
	Tea *newNode = createNode();
	Tea *temp = head;
	while(temp->next != NULL){
		temp = temp->next;
	}
	temp->next = newNode;
	newNode->next = NULL;
}
int nodeCount(){
	int count = 0;
	Tea *temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}
void addAtPosition(int pos){

	int count = nodeCount();
	if(pos <= 0 || pos >= count+2){
		printf("Invalid node position\n");
	}else if(pos == count+1){
		addLast();
	}else if(pos == 1){
		addFirst();
	}else{
		Tea *newNode = createNode();
		Tea *temp = head;
		while(pos-2){
			temp = temp->next;
			pos--;
		}
		newNode->next = temp->next;
		temp->next = newNode;
	}
}
void printLL(){

	Tea *temp = head;
	while(temp != NULL){
		printf("|%s->",temp->fName);
		printf("%d|",temp->noOfBranches);
		temp = temp->next;
	}
	printf("\n");
}
void deleteFirst(){

	if(head == NULL){
		printf("There is Nothing for delete\n");
	}else{
		Tea *temp = head;
		head = temp->next;
		free(temp);
	}
}
void deleteLast(){
	
	if(head == NULL){
		printf("There is Nothing for delete\n");
	}else if(head->next == NULL){
		free(head);
		head = NULL;
	}else{
		Tea *temp = head;

		while(temp->next->next != NULL){
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}
void deleteAtposition(int pos){
	int count = nodeCount();
	if(pos <= 0 || pos > count){
		printf("Invalid node position\n");
	}else if(pos == 1){
		deleteFirst();
	}else if(pos == count){
		deleteLast();
	}else{
		Tea *temp = head;
		while(pos-2){
			temp = temp->next;
			pos--;
		}
		free(temp->next->next);
		temp->next = NULL; 
	}
}
void main(){

	int noOfNodes;
	printf("How many nodes you want to create?:: ");
	scanf("%d",&noOfNodes);

	char choise;
	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPosition\n");
		printf("5.nodeCount\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		printf("8.deleteAtposition\n");
		printf("9.printLL\n");

		int num;
		printf("Enter your Choise:: ");
		scanf("%d",&num);

		switch(num) {

		case 1: 
			{
				for(int i=1; i<=noOfNodes; i++){
					addNode();
				}
			}
			break;
		case 2:
			addFirst();
			break;
		case 3:
			addLast();
			break;
		case 4:
			{
				int position;
				printf("Enter position for add node:: ");
				scanf("%d",&position);
				addAtPosition(position);
			}
			break;
		case 5:
			printf("Total no. of nodes present in the Linked List are %d\n",nodeCount());
			break;
		case 6:
			deleteFirst();
			break;
		case 7: 
			deleteLast();
			break;
		case 8:
			{
				int position;
				printf("Enter position for delete node:: ");
				scanf("%d",&position);
				deleteAtposition(position);
			}
			break;
		case 9:
			printLL();
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue::? [Y/y] ");
		scanf("%c",&choise);
	}while(choise == 'y' || choise == 'Y');
}