#include <stdio.h>
#include <string.h>

typedef struct PuneDams {

	char Name[20];
	int Distance;			  // from pune	
	float capacity;          // Water storage capacity in thousand million cubic feet (tmc)

	struct PuneDams *next;
}DAM;

void main(){

	DAM D1,D2,D3;

	DAM *head = &D1;

	strcpy(head->Name,"KHADAKWASALA");
	head->Distance = 15;
	head->capacity = 1.97;
	head->next = &D2;

	strcpy(head->next->Name,"PANSHET");
	head->next->Distance = 40;
	head->next->capacity = 10.65;
	head->next->next = &D3;

	strcpy(head->next->next->Name,"VARASGAON");
	head->next->next->Distance = 60;
	head->next->next->capacity = 12.73;
	head->next->next->next = NULL;

	printf("%s\n",head->next->next->Name);
	printf("%d\n",head->next->next->Distance);
	printf("%f\n",head->next->next->capacity);

	printf("%s\n",head->next->Name);
	printf("%d\n",head->next->Distance);
	printf("%f\n",head->next->capacity);

	printf("%s\n",head->Name);
	printf("%d\n",head->Distance);
	printf("%f\n",head->capacity);

}