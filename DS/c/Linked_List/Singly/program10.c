#include <stdio.h>
#include <stdlib.h>

typedef struct Movie {

	char mName[20];
	float imdb;
	struct Movie *next;
}MOV;

MOV *head = NULL;

void printLL(){

	MOV *temp = head;
	while(temp != NULL){

		puts(temp->mName);
		printf("%f ",temp->imdb);
		temp = temp->next;
	}
}
void addNode(){

	MOV *newNode = (MOV*)malloc(sizeof(MOV));
	
	printf("Enter Movie Name: ");
	fgets(newNode->mName,15,stdin);
	
	printf("Enter Movie Rating: ");
	scanf("%f",&newNode->imdb);
	
	if(head == NULL){
		head = newNode;
	}else{
		MOV *temp = head;
		while(temp->next != NULL){

			temp = temp->next;
		}
		temp->next = newNode;
	}
	newNode->next = NULL;
}
void main(){

	addNode();
	addNode();
	addNode();
	printLL();
}