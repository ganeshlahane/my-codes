#include <stdio.h>
#include <stdlib.h>

typedef struct Student {

	int Id;
	float Ht;
	struct Student *next; 
}Stud;

void addNode(Stud *head){

	Stud *newNode = (Stud*)malloc(sizeof(Stud));
	newNode->Id = 10;
	newNode->Ht = 5.5;
	newNode->next = NULL;

	head = newNode;
}

void printData(Stud *head){

	Stud *temp = head;

	while(temp != NULL){

		printf("%d ",temp->Id);
		printf("%f ",temp->Ht);
		temp = temp->next;
	}
	printf("\n");
}

void main(){

	Stud *head = NULL;

	addNode(head);
	printData(head);
}