// Real Time Example.

#include <stdio.h>
#include <stdlib.h>

typedef struct GanpatiTemple {

	struct GanpatiTemple *prev;
	char Tname[20];
	struct GanpatiTemple *next;
}Node;

Node *head = NULL;

Node* createNode(){

	Node *newNode =(Node*)malloc(sizeof(Node));
	getchar();
	newNode->prev = NULL;

	printf("Enter Temple name:: ");
	scanf("%[^\n]",newNode->Tname);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
	}
}

void addFirst(){

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}
}

void addLast(){

	Node *newNode = createNode();
	Node *temp = head;

	while(temp->next != NULL){

		temp = temp->next;
	}
	temp->next = newNode;
	newNode->prev = temp;
}

int countNode(){

	Node *temp = head;
	int count = 0;
	while(temp != NULL){
		count++;
		temp = temp->next;
	} 
	return count;
}

int addAtPosition(int pos){
	int count = countNode();
	if(pos<= 0 || pos > count ){
		printf("Invalid Input\n");
		return -1;
	}else{
		if(pos == count+1){
			addLast();
		}else if(pos == 1){
			addFirst();
		}else{
			Node *newNode = createNode();
			Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			newNode->prev = temp;
			temp->next->prev = newNode;
			temp->next = newNode;
		}
		return 0;
	}
}

void deleteFirst(){

	int count = countNode();
	if(head == NULL){
		printf("LL is Empty\n");
	}else{
		if(count == 1){
			free(head);
			head = NULL;
		}else{
			head = head->next;
			free(head->prev);
			head->prev = NULL;
		}
	}
}

void deleteLast(){

	int count = countNode();
	if(head == NULL){
		printf("LL is Empty\n");
	}else{
		if(count == 1){
			free(head);
			head = NULL;
		}else{
			Node *temp = head;
			while(temp->next->next != NULL){
				temp = temp->next;
			}
			free(temp->next);
			temp->next = NULL;
		}
	}
}

void deleteAtposition(int pos){

	int count = countNode();
	if(pos <= 0 || pos > count){
		printf("LL is Empty\n");
	}else{
		if(pos == 1){
			deleteFirst();
		}else if(pos == count){
			deleteLast();
		}else{
			Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}
	}
}
void printLL(){

	Node *temp = head;
	if(head == NULL){
		printf("LL is Empty\n");
	}else{
		while(temp->next !=  NULL){
			printf("|%s|<-->",temp->Tname);
			temp = temp->next;
		}
		printf("|%s|",temp->Tname);
		printf("\n");
	}
}

void main(){

	int noOfNodes;
	printf("How many nodes you want to create?:: ");
	scanf("%d",&noOfNodes);

	char choise;
	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPosition\n");
		printf("5.countNode\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		printf("8.deleteAtposition\n");
		printf("9.printLL\n");

		int num;
		printf("Enter your Choise:: ");
		scanf("%d",&num);

		switch(num) {

		case 1: 
			{
				for(int i=1; i<=noOfNodes; i++){
					addNode();
				}
			}
			break;
		case 2:
			addFirst();
			break;
		case 3:
			addLast();
			break;
		case 4:
			{
				int position;
				printf("Enter position for add node:: ");
				scanf("%d",&position);
				addAtPosition(position);
			}
			break;
		case 5:
			printf("Total no. of nodes present in the Linked List are %d\n",countNode());
			break;
		case 6:
			deleteFirst();
			break;
		case 7: 
			deleteLast();
			break;
		case 8:
			{
				int position;
				printf("Enter position for delete node:: ");
				scanf("%d",&position);
				deleteAtposition(position);
			}
			break;
		case 9:
			printLL();
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue::? [Y/y] ");
		scanf("%c",&choise);
	}while(choise == 'y' || choise == 'Y');
}

/*
OutPut :-
asus@Ganesh:~/DS/Linked_List/Doubly$ ./a.out
How many nodes you want to create?:: 8
1.addNode
2.addFirst
3.addLast
4.addAtPosition
5.countNode
6.deleteFirst
7.deleteLast
8.deleteAtposition
9.printLL
Enter your Choise:: 1
Enter Temple name:: Mayureshwar
Enter Temple name:: Sidhivinayak
Enter Temple name:: Ballaleshwar
Enter Temple name:: Varadhavinayak
Enter Temple name:: Chintamani
Enter Temple name:: Girijatmaj
Enter Temple name:: Vighneshwar
Enter Temple name:: Mahaganpati
Do you want to continue::? [Y/y] y
1.addNode
2.addFirst
3.addLast
4.addAtPosition
5.countNode
6.deleteFirst
7.deleteLast
8.deleteAtposition
9.printLL
Enter your Choise:: 9
|Mayureshwar|<-->|Sidhivinayak|<-->|Ballaleshwar|<-->|Varadhavinayak|<-->|Chintamani|<-->|Girijatmaj|<-->|Vighneshwar|<-->|Mahaganpati|
Do you want to continue::? [Y/y] n
*/