#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	struct Node *prev;
	int data;
	struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){

	Node* newNode = (Node*)malloc(sizeof(Node));

	newNode->prev = NULL;
	printf("Enter Data:: ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
int countNode(){

	Node* temp = head;
	int count = 0;
	while(temp->next != head){
		count++;
		temp = temp->next;
	}
	return count+1;
}
void addNode(){

	Node *newNode = createNode();
	if(head == NULL){

		head = newNode;
		head->prev = head;
		head->next = head;
	}else{

		head->prev->next = newNode;
		newNode->prev = head->prev;
		newNode->next = head;
		head->prev = newNode;   
	}
}
void addFirst(){

	Node *newNode = createNode();
	if(head == NULL){
		
		head = newNode;
		head->prev = head;
		head->next = head;
	}else{
		
		newNode->next = head;
		newNode->prev = head->prev;
		head->prev = newNode;
		head = newNode;
		head->prev->next = head;
	}
}
int addAtPosition(int pos){

	int count = countNode();
	if(pos <= 0 || pos > count+1){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count){
			addNode();
		}else{
			Node *newNode = createNode();
			Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next->prev = newNode;
			newNode->prev = temp;
			temp->next = newNode;
		}
		return 0;
	}
}
int deleteFirst(){

	if(head == NULL){
		printf("LL  is Empty\n");
		return -1;
	}else{
		if(head->next == head){
			
			free(head);
			head = NULL;
		}else{
			Node *temp = head;
			head->prev->next = head->next;
			head->next->prev = head->prev;
			head = head->next;
			free(temp);
		}
		return 0;
	}
}
int deleteLast(){

	if(head==NULL){
		printf("LL is Empty\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			head->prev->prev->next = head;
			free(head->prev);
			head->prev = head->next;
		}
		return 0;
	}
}
int deleteAtposition(int pos){

	int count = countNode();
	if(pos <= 0 || pos > count+1){
		printf("Invalid position");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{

			Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}
		return 0;
	}
}
int printLL(){

	Node *temp = head;
	if(head == NULL){
		printf("LL is Empty\n");
		return -1;
	}else{
		while(temp->next != head){
			printf("|%d|<-->",temp->data);
			temp = temp->next;
		}
		printf("|%d|",temp->data);
		printf("\n");
	}
}
void main(){

	int noOfNodes;
	printf("How many nodes you want to create?:: ");
	scanf("%d",&noOfNodes);

	char choise;
	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPosition\n");
		printf("4.deleteFirst\n");
		printf("5.deleteLast\n");
		printf("6.deleteAtposition\n");
		printf("7.printLL\n");
		printf("8.countNode\n");

		int num;
		printf("Enter your Choise:: ");
		scanf("%d",&num);

		switch(num) {

		case 1: 
			{
				for(int i=1; i<=noOfNodes; i++){
					addNode();
				}
				printLL();
			}
			break;
		case 2:
			addFirst();
			printLL();
			break;
		case 3:
			{
				int position;
				printf("Enter position for add node:: ");
				scanf("%d",&position);
				addAtPosition(position);
				printLL();
			}
			break;
		case 4:
			deleteFirst();
			printLL();
			break;
		case 5: 
			deleteLast();
			printLL();
			break;
		case 6:
			{
				int position;
				printf("Enter position for delete node:: ");
				scanf("%d",&position);
				deleteAtposition(position);
				printLL();
			}
			break;
		case 7:
			printLL();
			break;
		case 8:
			printf("%d\n",countNode());
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue::? [Y/y] ");
		scanf("%c",&choise);
	}while(choise == 'y' || choise == 'Y');
}