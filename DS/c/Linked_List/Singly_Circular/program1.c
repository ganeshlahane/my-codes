// singly circular LL templet.

#include <stdio.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;	
}Node;

Node* head = NULL;

Node* createNode(){

	Node *newNode =(Node*)malloc(sizeof(Node));

	printf("Enter Data:: ");
	scanf("%d",&newNode->data);


	newNode->next = head;
}
void addNode(){

	Node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		newNode->next = head;
	}else{
		Node *temp = head;
		while(temp->next != head){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->next = head;
	}
}
int countNode(){
	Node *temp = head;
	int count = 0;
	while(temp->next != head){
		count++;
		temp = temp->next;
	}
	count++;
	return count;
}
void addFirst(){

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
		newNode->next = head;
	}else{
		Node *temp = head;
		while(temp->next != head){
			temp=temp->next;
		}
		newNode->next = head;
		head = newNode;
		temp->next = head;
	}
}
int addAtPosition(int pos){

	int count = countNode();
	if(pos <= 0 || pos > count+1){
		printf("Invalid Positio\n");
		return -1;
	}else{
		if(pos == count+1){
			addNode();
		}else if(pos == 1){
			addFirst();
		}else{
			Node *newNode = createNode();
			Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
		return 0;
	}
}
int deleteFirst(){
	if(head == NULL){
		printf("LL is empty\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			Node *temp = head;
			while(temp->next != head){
				temp = temp->next;
			}
			head = head->next;
			free(temp->next);
			temp->next = head;
		}
		return 0;
	}
}
int deleteLast(){

	if(head==NULL){
		printf("LL is empty\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			Node *temp = head;
			while(temp->next->next != head){
				temp = temp->next;
			}
			free(temp->next);
			temp->next = head;
		}
		return 0;
	}
}
int deleteAtPosition(int pos){

	int count = countNode();
	if(pos <= 0 || pos > count+1){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos == count){
			deleteLast();
		}else if(pos == 1){
			deleteFirst();
		}else{
			Node *temp1 = head;
			Node *temp2 = head;
			while(pos-2){
				temp1 = temp1->next;
				temp2 = temp2->next->next;
				pos--;
			}
			free(temp1->next);
			temp1->next = temp2->next->next;
		}
		return 0;
	}
}
void printLL(){

	Node *temp = head;

	while(temp->next != head){
		printf("|%d|-->",temp->data);
		temp = temp->next;
	}
	printf("|%d|",temp->data);
	printf("\n");
}
void main(){

	int noOfNodes;
	printf("How many nodes you want to create?:: ");
	scanf("%d",&noOfNodes);

	char choise;
	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPosition\n");
		printf("4.countNode\n");
		printf("5.deleteFirst\n");
		printf("6.deleteLast\n");
		printf("7.deleteAtposition\n");
		printf("8.printLL\n");

		int num;
		printf("Enter your Choise:: ");
		scanf("%d",&num);

		switch(num) {

		case 1: 
			{
				for(int i=1; i<=noOfNodes; i++){
					addNode();
				}
			}
			break;
		case 2:
			addFirst();
			break;
		case 3:
			{
				int position;
				printf("Enter position for add node:: ");
				scanf("%d",&position);
				addAtPosition(position);
			}
			break;
		case 4:
			printf("Total no. of nodes present in the Linked List are %d\n",countNode());
			break;
		case 5:
			deleteFirst();
			break;
		case 6: 
			deleteLast();
			break;
		case 7:
			{
				int position;
				printf("Enter position for delete node:: ");
				scanf("%d",&position);
				deleteAtPosition(position);
			}
			break;
		case 8:
			printLL();
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue::? [Y/y] ");
		scanf("%c",&choise);
	}while(choise == 'y' || choise == 'Y');
}