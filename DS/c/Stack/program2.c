// implimenting stack using linked list

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;
}Node;

int Nodecount = 0;
Node *head = NULL;
Node *top = NULL;
int flag = 1;

int eleCount(){
	Node *temp = head;
	int count = 0;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}
Node* createNode(){
	Node* newNode = (Node*)malloc(sizeof(Node));
	
	printf("Enter Data:: ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}
void addNode(){
	Node *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void deleteLast(){
	Node *top = head;
	while(top->next->next != NULL){
		top = top->next;
	}
	free(top->next);
	top->next = NULL;
}
bool isEmpty(){
	if(eleCount() == 0){
		return true;
	}else{
		return false;
	}
}
int pop(){
	if(isEmpty() == Nodecount){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		deleteLast();
		return 0;
	}
}
bool isFull(){
	if(eleCount() == Nodecount){
		return true;
	}else{
		return false;
	}
}
int push(){
	if(isFull()){
		return -1;
	}else{
		addNode();
		return 0;
	}
}
int peek(){
	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		int val = top->data;
		flag = 1;
		return val;
	}
}
void main(){

	int noOfNodes;
	printf("Enter no. of element in stack:: ");
	scanf("%d",&noOfNodes);

	char choise;
	do{
		printf("1.push\n");
		printf("2.pop\n");
		printf("3.peek\n");

		int ch;
		printf("Enetr your choise:: ");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				{
					for(int i=0; i<noOfNodes; i++){
						push();
					}
				}
				break;
			case 2:
				{
					int ret = pop();
					if(flag==1){
						printf("%d is popped\n",ret);
					}
				}
				break;
			case 3:
				{
					int ret = peek();
					printf("%d is top value\n",ret);
				}
				break;
			default:
				printf("Wrong Choise\n");
				break;
		}
		getchar();
		printf("Do you want to continue [Y/y]:: ");
		scanf("%c",&choise);
	}while(choise == 'Y' || choise == 'y');


}