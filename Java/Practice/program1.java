// check given string is Pelindrome string or not.

import java.util.*;

class checkPalindromeString {

	static boolean check(String str){

		char arr[] = str.toCharArray();

		for(int i=0; i<arr.length/2; i++){

			if(arr[i] != arr[arr.length-i-1]){

				return false;
			}
		}
		return true;
	}
	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter String: ");
		String str = sc.next();

		boolean ch = check(str);

		if(ch == true){

			System.out.println(str+" is a Pelindromic String");
		}else{
			System.out.println(str+" is not a Pelindromic String");
		}
	}
} 
