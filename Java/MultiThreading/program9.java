// Dead Lock Scenario

class MyThread extends Thread {

	static Thread nmMain = null;

	//System.out.println(nmMain);

	public void run(){

		try{
			nmMain.join();
		}catch(InterruptedException ie){

		}
		for(int i=0; i<10; i++){

			System.out.println("In Thread");
		}
	}
}
class ThreadDemo {

	public static void main(String[] args)throws InterruptedException{

		MyThread.nmMain = Thread.currentThread();

		MyThread obj = new MyThread();
		obj.start();

		obj.join();

		for(int i=0; i<10; i++){

			System.out.println("In main");
		}
	}
}