// Creating thread by using thread classs (Use Sleep method)

class Mythread extends Thread {

	public void run(){

		for(int i=0; i<10; i++){

			System.out.println("In run");
			
			try{
				Thread.sleep(1000);
			}catch(InterruptedException ie){

			}
		}
	}
}  
class ThreadDemo {

	public static void main(String[] args)throws InterruptedException{

		Mythread obj = new Mythread();

		obj.start();

		for(int i=0; i<10; i++){

			System.out.println("In main");

			Thread.sleep(1000);
		}
	}
}