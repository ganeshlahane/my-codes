// don't override start method of Thread class

class Mythread extends Thread {

	public void run(){

		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}

	public void start(){

		System.out.println("In Mythread start");
		run();
	}
}
class ThraedDemo {

	public static void main(String[] args){

		Mythread obj = new Mythread();
		obj.start();

		System.out.println(Thread.currentThread().getName());
	}
}
