// ThreadGroup -- it is a Class

class MyThread extends Thread {

	MyThread(ThreadGroup tg, String str){

		super(tg,str);
	}

	public void run(){

		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo {

	public static void main(String[] args) {

		ThreadGroup pthreadGroup = 	new ThreadGroup("Core2Web");  // pthreadGroup = Parent Thread Group

		MyThread obj1 = new MyThread(pthreadGroup,"C");
		MyThread obj2 = new MyThread(pthreadGroup,"Java");
		MyThread obj3 = new MyThread(pthreadGroup,"Python");

		obj1.start();
		obj2.start();
		obj3.start();

		ThreadGroup ctrheadGroup = new ThreadGroup(pthreadGroup,"Incubator");

		MyThread obj4 = new MyThread(ctrheadGroup,"Flutter");
		MyThread obj5 = new MyThread(ctrheadGroup,"React");
		MyThread obj6 = new MyThread(ctrheadGroup,"SpringBoot");

		obj4.start();
		obj5.start();
		obj6.start();
	}
}
