// Conqurency methods in thread class
// sleep()

class MyThread extends Thread {

	public void run(){

		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo {

	public static void main(String[] args)throws InterruptedException{

		System.out.println(Thread.currentThread());

		MyThread obj = new MyThread();
		obj.start();

		Thread.sleep(2000);

		Thread.currentThread().setName("Ganesh");
		System.out.println(Thread.currentThread());

		MyThread obj2 = new MyThread();
		obj2.start();

		System.out.println(Thread.currentThread());
	}
}