// yield()

class MyThread extends Thread {

	public void run() {

		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadYeildDemo {

	public static void main(String[] args)throws InterruptedException {

		MyThread obj = new MyThread();
		obj.start();

		MyThread.yield();

		Thread.sleep(2000);

		System.out.println(Thread.currentThread().getName());
	}
}