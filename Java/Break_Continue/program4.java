
class ContinueDemo {

	public static void main(String[] g){

		int n = 50,cnt=0;

		for(int i=1; i<=n; i++){

			if((i%3==0 && i%5==0) || (i%4==0)){

				continue;
			}
			cnt++;
			System.out.println(i);
		}
		System.out.println("Number count = "+cnt);
	}
}