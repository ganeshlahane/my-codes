/*
	1	2	3	4
	4	5	6
	6	7
	7
*/
class Pattern {

	public static void main(String[] g){

		int n=1;
		for(int i=1; i<=4; i++){

			for(int j=i; j<=4; j++){

				System.out.print(n++ +"\t");
			}
			n--;
			System.out.println();
		}
	}
}