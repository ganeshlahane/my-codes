/*
	F
	E   F
	D   E   F
	C   D   E   F
	B   C   D   E   F
	A   B   C   D   E   F
*/
class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=6; i++){
			int n = 65+6-i;
			for(int j=1; j<=i; j++){

				System.out.print((char)n+"\t");
				n++;
			}
			System.out.println();
		}
	}
}