/*
	Write a Java program to merge two given arrays.
	Array1 = [10, 20, 30, 40, 50]	
	Array2 = [9, 18, 27, 36, 45]
	Output :
	Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
	Hint: you can take 3rd array
*/
import java.util.*;

class MergeArrays {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter First Array Size: ");

		int size1 = sc.nextInt();
		
		System.out.print("Enter Second Array Size: ");

		int size2 = sc.nextInt();

		int arr1[] = new int[size1];
		int arr2[] = new int[size2];
		int arr3[] = new int[size1+size2];

		System.out.println("Enter First Array Elements: ");

		for(int i=0; i<arr1.length; i++){

			arr1[i] = sc.nextInt();
			arr3[i] = arr1[i];
		}

		System.out.println("Enter Sencond Array Elements: ");

		for(int i=0; i<arr2.length; i++){

			arr2[i] = sc.nextInt();
			arr3[size1+i] = arr2[i];
		}

		System.out.print("Our First Array is = ");
		
		for(int i=0; i<arr1.length; i++){

			System.out.print("|"+arr1[i]);
		}
		System.out.println("|");

		System.out.print("Our Second Array is = ");
		
		for(int i=0; i<arr2.length; i++){

			System.out.print("|"+arr2[i]);
		}
		System.out.println("|");

		System.out.print("Our Merged Array is = ");
		
		for(int i=0; i<arr3.length; i++){

			System.out.print("|"+arr3[i]);
		}
		System.out.println("|");
	}
}