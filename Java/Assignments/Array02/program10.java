/*
	WAP to print the elements whose addition of digits is even.
	Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
	Input :
	Enter array : 1 2 3 5 15 16 14 28 17 29 123
	Output: 2 15 28 17 123
*/

import java.util.*;

class arrDemo {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt(); 
		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		for(int i=0; i<arr.length; i++){

			int sum=0,rem,num=arr[i];
			while(num != 0){

				rem = num%10;
				sum += rem;
				num = num/10; 
			}
			if(sum%2==0){

				System.out.println(arr[i]);
			}
		}
	}
}