/*
	WAP to search a specific element from an array and return its index.
	Input: 1 2 4 5 6
	Enter element to search: 4
	Output: element found at index: 2
*/
import java.util.*;

class SearchEle {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();

		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.print("Enter element to search = ");

		int search = sc.nextInt();

		int flag = 0;

		for(int i=0; i<arr.length; i++){

			if(search==arr[i]){
				flag = 1;
				System.out.println(search+" is found at index: "+i);
				break;
			}
		}
		if(flag == 0)
			System.out.println(search+" is not present in above array");
	}
}