/*
	WAP to find the common elements between two arrays.
	Input :	
	Enter first array : 1 2 3 5
	Enter Second array: 2 1 9 8
	Output: Common elements :
	1	
	2
*/
import java.util.*;

class FindCommon {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter First Array Elements: ");

		for(int i=0; i<arr1.length; i++){

			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter Sencond Array Elements: ");

		for(int i=0; i<arr2.length; i++){

			arr2[i] = sc.nextInt();
		}

		System.out.print("Our First Array is = ");
		
		for(int i=0; i<arr1.length; i++){

			System.out.print("|"+arr1[i]);
		}
		System.out.println("|");

		System.out.print("Our Second Array is = ");
		
		for(int i=0; i<arr2.length; i++){

			System.out.print("|"+arr2[i]);
		}
		System.out.println("|");

		int flag = 0;
		for(int i=0; i<arr1.length; i++){

			for(int j=0; j<arr1.length; j++){

				if(arr1[i] == arr2[j]){

					flag=1;
					System.out.println("Common element "+arr1[i]);
				}
			}
		}
		if(flag == 0){

			System.out.println("There is not present any common elements in both array");
		}
	}
}