/*
	Write a Java program to find the sum of even and odd numbers in an array.
	Display the sum value.
	Input: 11 12 13 14 15
	Output:
		Odd numbers sum = 39
		Even numbers sum = 26
*/
import java.util.*;

class SumOfEvenOdd {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		int Esum = 0,Osum = 0;

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();

			if(arr[i]%2==0){
				Esum += arr[i];
			}else{
				Osum += arr[i];
			}
		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.println("Sum of Even Elements = "+Esum);
		System.out.println("Sum of Odd Elements = "+Osum);
	}
}