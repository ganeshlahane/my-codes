/*
	WAP to take size of array from user and also take integer elements from user
	find the minimum element from the array
	input : Enter size : 5
	Enter array elements: 1 2 5 0 4
	output: min element = 0
*/
import java.util.*;

class FindMin {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		int min = 0;

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();

			if(arr[i]<min){

				min = arr[i];
			} 
		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.println("Minimum elements  = "+min);
	}
}