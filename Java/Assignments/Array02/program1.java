/*
	Write a program to create an array of ‘n’ integer elements.
	Where ‘n’ value should be taken from the user.
	Insert the values from users and find the sum of all elements in the array.
Input:
	n=6
Enter elements in the array: 2 3 6 9 5 1
Output:
	26
*/
import java.util.*;

class arrSum {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		int sum = 0;

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
			sum += arr[i]; 
		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.println("Sum of All numbers = "+sum);
	}
}