/*
WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
	Number of Even Elements: 4
	Number of Odd Elements : 3
*/

import java.util.*;

class FindEvenOdd {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		int ecnt = 0,ocnt = 0;

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();

			if(arr[i]%2==0){
				ecnt++;
			}else{
				ocnt++;
			}
		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.println("Number of Even Elements = "+ecnt);
		System.out.println("Number of Odd Elements = "+ocnt);
	}
}