/*
	F   5   D   3   B   1		
	F   5   D   3   B   1		
	F   5   D   3   B   1		
	F   5   D   3   B   1		
	F   5   D   3   B   1		
	F   5   D   3   B   1		
*/

class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=6; i++){
			int ch = 64+6;
			int n = 6;
			for(int j=1; j<=6; j++){

				if(j%2==1){
					System.out.print((char)ch+"\t");
				}else{
					System.out.print(n+"\t");
				}
				ch--;
				n--;
			}
			System.out.println();
		}
	}
}