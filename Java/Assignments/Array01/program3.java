/*
	WAP to take size of array from the user and also take elements from 
	the user and print the product of odd index only
*/

import java.util.*;

class arrProd {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		int prod = 1;

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();

			if(i%2==1){
				prod *= arr[i]; 
			}
		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.println("Product of element present at odd index = "+prod);
	}
}