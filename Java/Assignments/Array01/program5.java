/*
	WAP to take 10 numbers from the user and print only numbers that are divisible 
	by 5 
*/

import java.util.*;

class arrayDemo {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements:");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.println("Numbers divisible by 5 are = ");

		for(int i=0; i<arr.length; i++){

			if(arr[i]%5==0){

				System.out.println(arr[i]);
			}
		}
	}
}