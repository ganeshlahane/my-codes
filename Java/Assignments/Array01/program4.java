/*
	WAP to take character from the user and print only vouls
*/

import java.util.*;

class Char {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.next().charAt(0);

		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.print("Vowels in Above array = ");

		for(int i=0; i<arr.length; i++){

			if(arr[i]=='a' || arr[i]=='e' || arr[i]=='i' || arr[i]=='o' || arr[i]=='u'){

				System.out.print(arr[i]+" ");
			}			
		}
		System.out.println();
	}
}
