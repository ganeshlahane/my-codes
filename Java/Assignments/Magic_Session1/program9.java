// Reverse Given number.

class reverse {

	public static void main(String[] g){

		int N = 942111423,rev=0,rem=0;

		System.out.println("Given number = "+N);
		
		while(N != 0){

			rem = N % 10;

			rev = rev * 10 + rem;

			N = N / 10;
		}
		System.out.println("Reverse = "+rev);
	}
}