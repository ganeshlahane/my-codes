// WAP to calculate factorial of given number.

class Factorial {

	public static void main(String[] g){

		int n=5,fact=1;

		while(n>1){
			fact = fact*n;
			n--;
		}
		System.out.println("Factorial = "+fact);
	}
}