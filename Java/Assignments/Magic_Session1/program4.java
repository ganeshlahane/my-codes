// Print the count of odd digits of given number.

class countOddDigits {

	public static void main(String[] g){

		int num = 942111423,count = 0;

		while(num != 0){

			int rem = num % 10;
			if(rem%2 != 0){
				count++;
			}
			num = num / 10;
		}
		System.out.println("Count of odd digits = "+count);
	}
}