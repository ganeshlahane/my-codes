// Count the digits of given number.

class countDigits {

	public static void main(String[] g){

		int num = 942111423,count = 0;

		while(num != 0){

			count++;
			num = num / 10;
		}
		System.out.println("Count of digits = "+count);
	}
}