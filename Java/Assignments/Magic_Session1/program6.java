// WAP to print sum of all even numbers and multiplication of all odd numbers.

class Calculation {

	public static void main(String[] g){

		int num = 942111423,sum=0,mult=1;

		while(num != 0){

			int rem = num % 10;
			if(rem%2==0){
				sum += rem;
			}else{
				mult *= rem;
			}
			num = num/10;
		}
		System.out.println("Sum of even numbers = "+sum);
		System.out.println("Multiplication of odd numbers = "+mult);
	}
}