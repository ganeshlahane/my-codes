/*
	D4 C3 B2 A1
	A1 B2 C3 D4
	D4 C3 B2 A1
	A1 B2 C3 D4
*/

import java.io.*;
class Pattern {

	public static void main(String[] g)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter no. of rows: ");
		int rows = Integer.parseInt(br.readLine());

		int ch = 64+rows,num = rows;
		for(int i=1; i<=rows; i++){
			
			if(i%2==1){

				for(int j=1; j<=rows; j++){

					System.out.print((char)ch);
					System.out.print(num+" ");
					ch--;
					num--;
				}
				ch++;
				num++;
				System.out.println();
			}else{

				for(int j=1; j<=rows; j++){

					System.out.print((char)ch);
					System.out.print(num+" ");
					ch++;
					num++;
				}
				ch--;
				num--;
				System.out.println();	
			}
		}
	}
}
