/*
	WAP to print all even nubers in reverse order and odd numbers in the standard way within range.
	Take the start and end from the user.

	i/p:- start = 2
		  end = 9

	o/p:- 8642
	 	  3579
*/
import java.io.*;
class Pattern {

	public static void main(String[] g)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Starting number: ");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End number: ");
		int end = Integer.parseInt(br.readLine());

		for(int i=end; i>=start; i--){

			if(i%2==0)
					System.out.print(i+" ");
		}
		System.out.println();


		for(int i=start; i<=end; i++){

			if(i%2==1)
					System.out.print(i+" ");
		}
		System.out.println();
	}
}
