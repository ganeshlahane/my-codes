/*
	WAP to take two characters if these characters are equal then print them as it is
	but if they are  not equal then prrint there difference.

	i/p :- a p
	o/p :- 15
*/
import java.io.*;
class Pattern {

	public static void main(String[] g)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter First Character: ");
		char ch1 = (char)br.read();
		br.skip(1);

		System.out.println("Enter First Character: ");
		char ch2 = (char)br.read();
		br.skip(1);

		if(ch1==ch2){

			System.out.println((char)ch2+"="+(char)ch1);
		}else{

			if(ch1>ch2){

				int diff = 0;
				diff = ch1-ch2;					
				System.out.println("Difference between "+ch1+" and "+ch2+" is = "+diff);
			}else{

				int diff = 0;
				diff = ch2-ch1;					
				System.out.println("Difference between "+ch1+" and "+ch2+" is = "+diff);
			}
		}
	}
}
