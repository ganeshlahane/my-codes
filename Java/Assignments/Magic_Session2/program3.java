/*
	5 4 3 2 1
	8 6 4 2
	9 6 3
	8 4
	5
*/
import java.io.*;
class Pattern {

	public static void main(String[] g)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter no. of rows: ");
		int rows = Integer.parseInt(br.readLine());

		for(int i=1; i<=rows; i++){
			int num = rows-i+1;
			for(int j=i; j<=rows; j++){

				System.out.print(num*i+" ");
				num--;
			}
			System.out.println();
		}
	}
}
