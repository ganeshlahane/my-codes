/*
	Program 1
	Write a program to print count of digits in elements of array.
	Input: Enter array elements : 02 255 2 1554
	Output: 2 3 1 4
*/

import java.util.*;

class DigiCount {

	int count(int x){

		int cnt = 0;
		while(x != 0){

			cnt++;
			x = x/10;
		}
		return cnt;
	}
	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.println("Enter Array Elements:");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		DigiCount obj = new DigiCount();

		for(int i=0; i<arr.length; i++){

			System.out.print(obj.count(arr[i])+" ");
		}
		System.out.println();
	}
}