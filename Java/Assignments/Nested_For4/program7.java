/*
	F
	E   1
	D   2   E
	C   3   D   4
	B   5   C   6   D
	A   7   B   8   C   9
*/
class Pattern {

	public static void main(String[] g){

		int n=1;
		for(int i=1; i<=6; i++){
			int ch = 65+6-i;
			for(int j=1; j<=i; j++){

				if(j%2==1){
					System.out.print((char)ch++ +"\t");
				}else{
					System.out.print(n++ +"\t");
				}
			}
			System.out.println();
		}
	}
}