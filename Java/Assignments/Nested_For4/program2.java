/*
	1
	3	4
	6	7	8
	10	11	12	13
	15	16	17	18	19
*/

class Pattern {

	public static void main(String[] g){

		int n=1;
		for(int i=1; i<=5; i++){

			for(int j=1; j<=i; j++){

				System.out.print(n++ +"\t");
			}
			n++;
			System.out.println();
		}
	}
}