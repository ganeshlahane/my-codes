/*
	1	
	8	9	
	9	64	25
	64	25	216  49
*/
class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=4; i++){
			int n=i;
			for(int j=1; j<=i; j++){

				if(n%2==0){
					System.out.print(n*n*n+"\t");
				}else{
					System.out.print(n*n+"\t");
				}
				n++;
			}
			System.out.println();
		}
	}
}