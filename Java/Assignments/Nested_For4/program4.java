/*
	1	2	3	4	
	2	3	4
	3	4
	4
*/
class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=4; i++){
			int n=i;
			for(int j=i; j<=4; j++){

				System.out.print(n++ +"\t");
			}
			System.out.println();
		}
	}
}