/*
	1
	8	9
	27	16	125	
	64	125	216	 49
*/
class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=4; i++){
			int n=i;
			for(int j=1; j<=i; j++){

				if(j%2==1){
					System.out.print(n*n*n+"\t");
				}else{
					System.out.print(n*n+"\t");
				}
				n++;
			}
			n--;
			System.out.println();
		}
	}
}