/*
	10
	I   H
	7	6	5
	D   C   B   A
*/

class Pattern {

	public static void main(String[] g){

		int n=10,ch=64+10;
		for(int i=1; i<=4; i++){

			for(int j=1; j<=i; j++){

				if(i%2==1){

					System.out.print(n+"\t");
				}else{

					System.out.print((char)ch+"\t");
				}
				n--;
				ch--;
			}
			System.out.println();
		}
	}
}