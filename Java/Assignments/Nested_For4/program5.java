/*
	A  B  C  D
	B  C  D
	C  D
	D
*/
class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=4; i++){
			int n=64+i;
			for(int j=i; j<=4; j++){

				System.out.print((char)n++ +"\t");
			}
			System.out.println();
		}
	}
}