/*
	1	
	2	3
	3	4	5
	4	5	6	7
*/
class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=4; i++){
			int n=i;
			for(int j=1; j<=i; j++){

				System.out.print(n++ +"\t");
			}
			n--;
			System.out.println();
		}
	}
}