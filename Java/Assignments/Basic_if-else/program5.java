// - Write a java program, in which according to month no print the no. of days in that month.

class Month {

	public static void main(String[] g){

		int monthNo = 5;

		if(monthNo == 1 ){
			System.out.println("Jan has 31 Days");
		}else if(monthNo == 2){
			System.out.println("Feb has 28 Days");
		}else if(monthNo == 3){
			System.out.println("March has 31 Days");
		}else if(monthNo == 4){
			System.out.println("April has 30 Days");
		}else if(monthNo == 5){
			System.out.println("May has 31 Days");
		}else if(monthNo == 6){
			System.out.println("June has 30 Days");
		}else if(monthNo == 7){
			System.out.println("July has 31 Days");
		}else if(monthNo == 8){
			System.out.println("Augest has 31 Days");
		}else if(monthNo == 9){
			System.out.println("Sept has 30 Days");
		}else if(monthNo == 10){
			System.out.println("Oct has 31 Days");
		}else if(monthNo == 11){
			System.out.println("Nove has 30 Days");
		}else if(monthNo == 12){
			System.out.println("Dec has 31 Days");
		}else{
			System.out.println("Invalid Input");
		}
	}
}