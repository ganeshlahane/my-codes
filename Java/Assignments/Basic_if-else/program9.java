// WAP to accept three numbers and check whether they are Pythagorean triplets or not.

class Pythagorus {

	public static void main(String[] g){

		int a = 4;
		int b = 3;
		int c = 5;

		if(a*a + b*b == c*c){
			System.out.println("Its Pythagorean Triplet");
		}else if(b*b + c*c == a*a){
			System.out.println("Its Pythagorean Triplet");
		}else if(c*c + a*a == b*b){
			System.out.println("Its Pythagorean Triplet");
		}else{
			System.out.println("Its not a Pythagorean Triplet");
		}
	}
}