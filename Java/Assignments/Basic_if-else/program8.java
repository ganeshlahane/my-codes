//write a program to check day number(1-7) and print the corrosponding day of week.

class Days {

	public static void main(String[] g){

		int num = 7;

		if(num == 0 || num > 7){
			System.out.println("Invalid Input");
		}else if(num == 1){
			System.out.println("Monday");
		}else if(num==2){
			System.out.println("Tuesday");
		}else if(num==3){
			System.out.println("Wednesday");
		}else if(num==4){
			System.out.println("Thursday");
		}else if(num==5){
			System.out.println("Friday");
		}else if(num==6){
			System.out.println("Saturday");
		}else{
			System.out.println("Sunday");
		}
	}
}