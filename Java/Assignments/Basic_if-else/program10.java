// Write a unique real time example of if-else ladder.

class realTimeExample {

	public static void main(String[] Trekking){

		int budget = 1000;

		if(budget <= 500){

			System.out.println("Go to Sinhgad");
		
		}else if(budget > 500 && budget <= 1000){

			System.out.println("Go to Rajgad");
		
		}else if(budget > 1000 && budget <= 1500){

			System.out.println("Go to Pratapgad");
		
		}else if (budget > 1500 && budget <= 2000){

			System.out.println("Go to Raigad");
		
		}else{

			System.out.println("Trekking Cancel");
		}
	}
}