class Parent {

	Parent(){

		System.out.println("In Parent Contructor");
	}

	void parentProperty(){

		System.out.println("Car,Flat,Gold");
	}
}
class Child extends Parent {

	Child(){

		System.out.println("In Child Contructor");
	}
}
class Client {

	public static void main(String[] g){

		Child obj = new Child();
		obj.parentProperty();
	}
}