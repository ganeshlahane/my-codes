// Ternary Operator (? :) it has 3 oparands

class Ternary {

	public static void main(String[] args){

		int x = 10;
		int y = 20;

		System.out.println((x<y)?x:y); // 10
		System.out.println((x<y)?y:x); // 20

	}
}