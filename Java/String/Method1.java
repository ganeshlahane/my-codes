// method : compareTo(String str);

import java.util.*;
class CompareToDemo {

	static int myCompareTo(String str1, String str2){

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		int flag = 0;

		if(flag == 0){

			for(int i=0; i<arr1.length; i++){

				if(arr1[i] != arr2[i]){
					return arr1[i]-arr2[i];
				}
			}
			flag = 1;
		}
		if(flag == 1){

			if(arr1.length != arr2.length){

				return arr1.length-arr2.length;
			
			}else{

				return 0;
			}
		}
		return 0;
	}
	public static void main (String[] g){

		Scanner sc = new Scanner(System.in);

		String str1 = sc.next();
		String str2 = sc.next();

		int ans = myCompareTo(str1,str2);

		System.out.println(ans);
		//System.out.println(str1.compareTo(str2));
	}
}
