// method : public int indexOf(char ch, int fromIndex);

import java.util.*;
class IndexOfImpli {

	static int myIndexOf(String str,char ch,int from){

		char arr[] = str.toCharArray();

		for(int i=from; i<arr.length; i++){

			if(arr[i] == ch){
				return i;
			}
		}
		return 0;
	}
	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter String : ");
		String str = sc.next();

		System.out.println("String = "+str);

		System.out.print("Enter character for finding its index: ");
		char ch = sc.next().charAt(0);

		System.out.print("Enter from which index you want to search: ");
		int from = sc.nextInt();

		int index = myIndexOf(str,ch,from);

		if(index==0){
			System.out.println("Character '"+ch+"' is not present in the given string");
		}else{
			System.out.println("Character '"+ch+"' is present at "+index+" index");
		}
	}
}