
class StringDemo {

	public static void main(String[] g){

		String str1 = "Ganesh";                   // goes on String Constant Pool (SCP)

		String str2 = new String("Ganesh");		  // goes on Heap Secssion

		char str3[] = {'G','a','n','e','s','h'};  // goes on Heap Secssion

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}