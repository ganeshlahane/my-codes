class StringDemo {

	public static void main(String[] g){

		String str1 = "Ganesh";                   // goes on String Constant Pool (SCP)

		String str2 = new String("Ganesh");		  // goes on Heap Secssion

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));

		String str3 = "Ganesh";
		String str4 = new String("Ganesh");

		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}
