// method : public boolean equals(Object anObject);

import java.util.*;
class EqualsImpli {

	static boolean myEquals(String str1,String str2){

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		if(arr1.length == arr2.length){

			for(int i=0 ; i<arr1.length; i++){

				if(arr1[i] != arr2[i]){

					return false;
				}
			}
			return true;
		}else{

			return false;
		}
	}
	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		String str1 = sc.next();
		String str2 = new String(sc.next());

		boolean ans = myEquals(str1,str2);
		
		System.out.println(ans);
		//System.out.println(str1.equals(str2));
	}
}
