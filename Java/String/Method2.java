// method : concat(string str);

import java.util.*;
class ConcatImpli {

	static String myConact(String str1, String str2){

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();
		
		char arr3[] = new char[arr1.length+arr2.length];

		for(int i=0; i<arr1.length; i++){

			arr3[i] = arr1[i];
		} 
		for(int i=0; i<arr2.length; i++){

			arr3[arr1.length+i] = arr2[i];
		}

		String str = new String(arr3);
		return str;
	}

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		String str1 = sc.nextLine();
		String str2 = sc.nextLine();

		String str3 = myConact(str1,str2);

		System.out.println(str3);
		System.out.println(str1.concat(str2));
	}
}