import java.io.*;
class IODemo {

	public static void main(String[] g)throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);

		BufferedReader br1 = new BufferedReader(isr);

		String str1 = br1.readLine();
		System.out.println("String1 = "+str1);

		br1.close();                                  // it throws IOException

		//char ch = (char)isr.read();
		//System.out.println("char = "+ch);
	}
}