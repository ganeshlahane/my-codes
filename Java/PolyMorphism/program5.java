class Parent {

	Parent() {

		System.out.println("In Parent Constructor");
	}

	void property() {

		System.out.println("Home,Car,Gold");
	}

	void marry() {

		System.out.println("Kriti Senan");
	}
}

class Child extends Parent {

	Child() {

		System.out.println("In Child Constructor");
	}

	void marry(){

		System.out.println("SHRADHA KAPOOR");
	}
}

class Client {

	public static void main(String[] args){

		Parent obj = new Child();              // or Child obj = new Child();

		obj.property();
		obj.marry();
	}
}
