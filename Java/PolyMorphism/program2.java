class Demo {

	int fun(int x) {

		System.out.println(x);
	}

	float fun(int x) {
	
		System.out.println(x);
	}
}

/*
	error : method fun(int) is already definded in class demo

	same error occur even we change the datatype of method
*/