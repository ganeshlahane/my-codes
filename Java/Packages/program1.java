import java.util.*;
import arithfun.Addition;

class Client {

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter First Number ::");
		int x = sc.nextInt();

		System.out.println("Enter Second Number ::");
		int y = sc.nextInt();

		Addition obj = new Addition(x,y);

		System.out.println("Sum = "+obj.add());

		sc.close();
	}
}