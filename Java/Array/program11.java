// passing array an argument

class PassingArray {

	public static void main(String[] g){

		int arr[] = {10,20,30,40,50};

		PassingArray obj = new PassingArray();

		for(int x: arr){
			System.out.println(x);
		}

		obj.fun(arr);

		System.out.println("===================");

		for(int x: arr){
			System.out.println(x);
		}
	}

	void fun(int[] arr){

		arr[1] = 70;
		arr[2] = 80;
	}
}