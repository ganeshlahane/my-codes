class JaggedArray {

	public static void main(String[] g){

		int arr[][] = {{1,2,3},{4,5},{6}};

		for(int i=0; i<arr.length; i++){

			for(int j=0; j<arr.length; j++){     // Error : ArrayIndexOutOfBoundsException 

				System.out.print(arr[i][j]+" ");
			}
			System.out.print("\n");
		}
	} 	
}