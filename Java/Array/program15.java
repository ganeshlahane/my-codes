
class TwoDArray {

	public static void main(String[] g){

		int arr1[][] = new int [2][2];
		int arr2[]   = new int [2];

		System.out.println(arr1.length); // 2   in two array it gives row count as length
		System.out.println(arr2.length); // 2

		arr1[0][0] = 10;
		arr1[0][1] = 10;
		arr1[1][0] = 10; 
		arr1[1][1] = 10;

		System.out.println(arr1[1][1]);
		System.out.println(arr1[0]);
		System.out.println(arr1[1]);
		System.out.println(arr1);
	}
}