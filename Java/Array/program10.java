// for each loop

class ForEach {

	public static void main(String[] g){

		int arr[] = {10,20,30,40,50};

		for(int i=0; i<arr.length; i++){

			System.out.println(arr[i]);
		}
		System.out.println("=============");

		for(int x: arr){

			System.out.println(x);
		}
	}
}