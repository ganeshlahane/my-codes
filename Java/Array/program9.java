// Integer cache (Autobaxing)

class IntegerCache {

	public static void main(String[] g){

		int x = 10;
		int y = 10;

		Integer z = 10;

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(z));
	}
}