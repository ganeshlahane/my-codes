// print the count of even numbers

import java.util.*;

class EvenCount {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		int count = 0;

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();

			if(arr[i]%2==0){

				count++;
			} 
		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.println("Count of even elements = "+count);	
	}
}