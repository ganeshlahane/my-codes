// user input by using BufferedReader

import java.io.*;

class ArrayDemo {

	public static void main(String[] g)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size:");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<size; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Our array is : ");;

		for(int i=0; i<size; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");
	}
}