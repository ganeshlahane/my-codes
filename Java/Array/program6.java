// Sum of array elements

import java.util.*;

class arrSum {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size: ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		int sum = 0;

		System.out.println("Enter Array Elements: ");

		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();

			sum += arr[i]; 
		}
		System.out.print("Our Array is = ");

		for(int i=0; i<arr.length; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");

		System.out.println("Sum of array elements = "+sum);
	}
}