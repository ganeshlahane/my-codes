// user input by using scanner

import java.util.*;
class ArrayDemo {

	public static void main(String[] g){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of Array:");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter Array Elements:");

		for(int i=0; i<size; i++){

			arr[i] = sc.nextInt();
		}

		System.out.print("Our array is :");

		for(int i=0; i<size; i++){

			System.out.print("|"+arr[i]);
		}
		System.out.println("|");
	}
}