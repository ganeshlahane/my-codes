/*
	A 1 B 2
	A 1 B 2
	A 1 B 2
	A 1 B 2
*/

class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=4; i++){
			int n=1;
			char ch = 'A';
			for(int j=1; j<=4; j++){

				if(j%2==1){
					System.out.print(ch+" ");
					ch++;
				}else{
					System.out.print(n+" ");
					n++;
				}
			}
			System.out.println();
		}
	}
}