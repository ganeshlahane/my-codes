/*
	1
	1 2 
	1 2 3 
	1 2 3 4
*/
class Pattern {

	public static void main(String[] g){

		for(int i=1; i<=5; i++){
			int n=1;
			for(int j=1; j<=i; j++){

				System.out.print(n++ +"\t");
			}
			System.out.println();
		}
	}
}