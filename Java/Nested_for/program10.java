/*
	A 1 B 2
	C 3 D 4
	E 5 F 6
*/

class pattern {

	public static void main(String[] g){

		int n = 1;
		char ch = 'A';
		for(int i=1; i<=3; i++){
			
			for(int j=1; j<=4; j++){

				if(j%2==1){
					System.out.print(ch+" ");
					ch++;
				}else{
					System.out.print(n+" ");
					n++;
				}
			}
			System.out.println();
		}
	}
}