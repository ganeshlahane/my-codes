// print numbers which divisible by 4 till N

class Divisible {

	public static void main(String[] g){

		int N = 20, i = 1;

		while(i<=N){

			if(i % 4 == 0){
				System.out.println(i);
			}
			i++;
		}

	}
}