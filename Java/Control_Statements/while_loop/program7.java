// Print the sum of all digits from the given number.

class SumOfDigits {

	public static void main(String[] g){

		int Num = 6531,rem=0,sum=0;

		while(Num != 0){

			rem = Num%10;
			sum += rem;
			Num = Num/10;
		}
		System.out.println(sum);
	}
}