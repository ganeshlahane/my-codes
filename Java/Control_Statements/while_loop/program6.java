// Print the all digits from the given number.

class PrintDigits {

	public static void main(String[] g){

		int Num = 6531,rem=0;

		while(Num != 0){

			rem = Num%10;
			System.out.println(rem);
			Num = Num/10;
		}
	}
}