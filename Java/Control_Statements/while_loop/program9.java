// Print the perfect square root till N integers.

class PerfectSquare {

	public static void main(String[] g){

		int N = 30,i=1,num=0;

		while(i*i <= N){

			num = i*i;
			if(num/i==i && num%i==0){
				System.out.println(i*i);
			}
			i++;
		}
	}
}