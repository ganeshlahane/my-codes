// Print the Product of all digits from the given number.

class ProdOfDigits {

	public static void main(String[] g){

		int Num = 6531,rem=0,prod=1;

		while(Num != 0){

			rem = Num%10;
			prod *= rem;
			Num = Num/10;
		}
		System.out.println(prod);
	}
}