// print odd numbers till N.

class Odd {

	public static void main(String[] g){

		int N = 10, i=1;

		while(i<=N){

			if(i % 2 != 0){
				System.out.println(i);
			}
			i++;
		}
	}
}