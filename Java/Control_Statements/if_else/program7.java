/*
Print the number is divisible by 3 and 5,
Number is fizz if it is divisible by 3
Number is buzz if it is divisible by 5
Number is fizz-buzz if it is divisible by both 3 and 5
if not is not divisible.
*/

class Divisible {

	public static void main(String[] g){

		int num = 15;

		if(num%3==0 && num%5==0){
			System.out.println(num+" is fizz-buzz");
		}else if(num%3==0){
			System.out.println(num+" is fizz");
		}else if(num%5==0){
			System.out.println(num+" is buzz");
		}else{
			System.out.println(num+" is not Divisible by 3 and 5");
		}
	}
}