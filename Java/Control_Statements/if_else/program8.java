/* Calculate Electricity bill.
   unit = 100 // price per unit 1 rupee
   o/p = 100

   unit = 200 // price per unit after first 100 unit is 2 rupee
   o/p  = 300
*/

class Bill {

	public static void main(String[] g){

		int unit = 200;

		if(unit <= 100){
			System.out.println(unit*1);
		}else{
			System.out.println(100*1+(unit-100)*2);
		}
	}
}