/*
	-Given the temperature of person in farenheit.
	-Printf whether the person has High,Normal,Low temperature,
*/

class Temp {

	public static void main(String[] args){

		float temp = 97.0f;

		if(temp>98.6)
			System.out.println("High");
		else if(temp>=98.0)
			System.out.println("Normal");
		else
			System.out.println("Low");
	}
}