//ArrayList

import java.util.*;

class ArrayListDemo {

	public static void main(String[] args) {

		ArrayList al = new ArrayList();

		// Method 1) add();

		al.add(10);
		al.add(20.5f);
		al.add("Ganesh");
		al.add(10);
		al.add(20.5f);

		ArrayList al2 = new ArrayList();

		al2.add("CSK");
		al2.add("MI");
		al2.add("GT");

		System.out.println(al);

		// Method 2) size();

		System.out.println(al.size());

		// Method 3) boolean contains(java.lang.Object);

		System.out.println(al.contains("Ganesh"));

		// Method 4) int indexOf(java.lang.Object);

		System.out.println(al.indexOf("Ganesh"));

		// Method 5) int lastIndexOf(Object);

		System.out.println(al.lastIndexOf(20.5f));

		// Method 6) E get(int);

		System.out.println(al.get(2));

		// Method 7) E set(int,E);

		al.set(3,"Lahane");
		System.out.println(al);

		// Method 8) void add(int,E);

		al.add(3,"Datta");
		System.out.println(al);

		// Method 9) addAll(Collection);

		al.addAll(al2);
		System.out.println(al);

		// Method 10) boolean addAll(Collection);

		al.addAll(2,al2);
		System.out.println(al);

		// Method 11) E remove(int);

		System.out.println(al.remove(3));
		System.out.println(al);

		// void Clear();

		al.clear();
		System.out.println(al);
	}
}