// if-else Ladder

#include <stdio.h>

void main(){
	int pmoney;
	printf("Enter Your Budget: ");
	scanf("%d",&pmoney);

	if(pmoney>=2500){
		printf("Happypola\n");
	}else if(pmoney>=2000){
		printf("CO2\n");
	}else if(pmoney>=1000){
		printf("Sarovar\n");
	}else if(pmoney>=500){
		printf("Vaishali Dosa\n");
	}else{
		printf("Plan Cancel\n");
	}
}