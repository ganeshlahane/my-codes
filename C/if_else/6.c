
// if else statement take boolian values or work on boolian values, i.e True or False

#include <stdio.h>

void main(){
	printf("Start Main\n");
	int x = 0;
	int y = 20;
	if(x){
		printf("In first if-block\n");
	}
	if(y){
		printf("In second if-block\n");
	}
	printf("End Main\n");
}