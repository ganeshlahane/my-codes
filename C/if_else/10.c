#include <stdio.h>

void main(){
	int x = 1;
	printf("Start main\n");
	if(--x){
		printf("In first block\n");
	}
	if(++x || x++){
		printf("In Second Block\n");
	}
	printf("%d\n",x);
	printf("End Code\n");
}