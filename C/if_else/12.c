/* if-else Statement:
					
					write a program to print given number is divisible by 4 and 5 or not.*/


#include <stdio.h>

void main(){
	int x;
	printf("Enter Value: ");
	scanf("%d",&x);

	if(x%4==0 && x%5==0){
		printf("Given no. is divisible by 4 & 5\n");	
	}else{
		printf("Given no. is not divisible by 4 & 5\n");
	}
}