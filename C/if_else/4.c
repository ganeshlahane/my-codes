// if statement example:

#include <stdio.h>

void main(){

	float income;

	printf("Enter Your income: ");
	scanf("%f",&income);
	printf("Your income is = %f\n",income);

	if(income>=0000 && income<=500000){
		printf("And you have 0% Tax on your income\n");
	}
	if(income>=500001 && income<=1000000){
		printf("And you have 10% Tax on your income\n");
	}
	if(income>=1000001 && income<=1500000){
		printf("And you have 15% Tax on your income\n");
	}
  	if(income>=1500001 && income<=2000000){
		printf("And you have 20% Tax on your income\n");
	}
	if(income>2000000){
		printf("And you have 30% Tax on your income\n");
	}
}