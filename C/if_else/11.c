/* if-else Statement:
					
					write a program to print given number is divisible by 2 or not.*/


#include <stdio.h>

void main(){
	int x;
	printf("Enter Value: ");
	scanf("%d",&x);

	if(x%2==0){
		printf("Given no. is divisible by 2\n");	
	}else{
		printf("Given no. is not divisible by 2\n");
	}
}