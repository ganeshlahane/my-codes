// if-else Ladder. another Example

#include <stdio.h>

void main(){
	int pmoney;
	printf("Enter your budget: ");
	scanf("%d",&pmoney);

	if(pmoney>=2000){
		printf("We all go to PANHALA FORT\n");
	}else if(pmoney>=1000){
		printf("We all go to PRATAPGAD\n");
	}else if(pmoney>=500){
		printf("We all go to SINGHGAD\n");
	}else{
		printf("Trip Cancel\n");
	}
}