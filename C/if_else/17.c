/* switch case. 
   its works as same as if-else.
   but it allows only two datatypes that is int and char.
   it alway integer values.*/

#include <stdio.h>

void main(){
	int x;
	printf("Enter values between 1-5: ");
	scanf("%d",&x);

	switch(x){
		case 1 :
		printf("ONE\n");
		break;
		case 2 :
		printf("TWO\n");
		break;
		case 3 :
		printf("THREE\n");
		break;
		case 4 :
		printf("FOUR\n");
		break;
		case 5 :
		printf("FIVE\n");
		break;
		default:
		printf("Invalid Input\n");
	}
}
