
// Real time example of "if Statement"
#include <stdio.h>

void main(){

	int tickPrice;

	printf("Enter Ticket Prices: ");	
	scanf("%d",&tickPrice);
	printf("Ticket Price = %d\n",tickPrice);

	if(tickPrice == 150){
		printf("You can book your Seat in [ROYAL SILVER] Section\n");
	}
        if(tickPrice == 250){
		printf("You can book your Seat in [ROYAL GOLD] Section\n");
	}
	if(tickPrice == 350){
		printf("You can book your Seat in [ROYAL SOFA] Section\n");
	}
}
