//6. WAP to calculate the factorial of a given number.

#include <stdio.h>

void main(){

	int num,fact=1;
	printf("Enter number: ");
	scanf("%d",&num);

	for(int i=num; i>=1; i--){

		fact = fact*i;
	}
	printf("Factorial of %d is = %d\n",num,fact);
}