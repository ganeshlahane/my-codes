// 1. WAP a program to see a given number is a multiple of 3.

#include <stdio.h>

void main(){

	int num;
	printf("Enter number: ");
	scanf("%d",&num);

	for(int i=1; i<=num; i++){

		if(i%3==0){

			printf("%d\n",i);
		}
	}
}