// 2. WAP to print the character whose ASCII is even.

#include <stdio.h>

void main(){

	for(char ch='A'; ch<='Z'; ch++){

		if(ch%2==0){
			printf("%c\n",ch);
		}
	}
}