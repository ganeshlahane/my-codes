/*A B C D
  B C D E
  C D E F
  D E F G*/

#include <stdio.h>

void main(){

	char ch = 'A';
	int rows;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			printf("%c  ",ch);
			ch++;
		}ch-=3;
		printf("\n");
	}
}
