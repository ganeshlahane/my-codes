/* 	1	2	3	4
	2	3	4	5
	3	4	5	6
	4	5	6	7	*/

#include <stdio.h>

void main(){

	int rows;
	printf("Enter No. of rows: ");
	scanf("%d",&rows);
	int x=1;	
	for(int i=0; i<rows; i++){
		
		for(int j=0; j<rows; j++){
			printf("%d ",x);
			x++;
		}x-=3;
		printf("\n");
	}
}
