/*  	1	2	3	
	a	b	c	
	1	2	3
	a	b	c	*/

#include <stdio.h>

void main(){

	int rows;
	printf("Enter No. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){
		int y = 97,x = 1;
		for(int j=0; j<rows; j++){

			if(i%2==0){
				printf("%d ",x);
				x++;
			}else{
				printf("%c ",y);
				y++;
			}
		}printf("\n");
	}
}
