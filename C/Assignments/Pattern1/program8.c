/*1  3  5
  7  9  11
  13 15 17
*/

#include <stdio.h>

void main(){

	int num=1,rows;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			printf("%d ",num);
			num+=2;
		}printf("\n");
	}
}
