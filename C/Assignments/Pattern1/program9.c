/*2  5  10
  17 26 37
  50 65 82 */

#include <stdio.h>

void main(){

	int num=1,rows;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			printf("%d ",num*num+1);
			num+=1;
		}printf("\n");
	}
}
