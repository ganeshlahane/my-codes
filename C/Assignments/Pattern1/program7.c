/*1   2  9   4
  25  6  49  8
  81  10 121 12
  169 14 225 16
*/

#include <stdio.h>

void main(){

	int rows,num=1;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			if(j%2==0){
				printf("%d ",num*num);
				num++;
			}else{
				printf("%d ",num);
				num++;
			}	
		}printf("\n");
	} 
}
