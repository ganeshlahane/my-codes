/*2. WAP to calculate the count of even and odd elements
Take array size and array elements from the user
IP : enter array : 10 12 13 15 16 17 19 20 22 23
OP: even element count is
OP: odd element count is  */

#include <stdio.h>

void main(){

	int size;
	printf("Enter array size: ");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements: \n");

	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}
	int Ecount=0,Ocount=0;
	for(int i=0; i<size; i++){

		if(arr[i]%2==0){
			Ecount++;
		}else{
			Ocount++;
		}
	}
	printf("Even number count is: %d\n",Ecount);
	printf("Odd number count is: %d\n",Ocount);
}