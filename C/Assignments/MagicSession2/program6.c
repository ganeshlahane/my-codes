/*6. WAP to swap values of two numbers using a pointer.
(Hint: Use de-referencing of pointers)
Input : x=10
y=20
Op: After swapping
x=20
y=10
*/

#include <stdio.h>

void main(){

	int x=10;
	int y=20;
	int *iptr=&x;
	int *cptr=&y;
	

	printf("Befor Swaping: \n");
	printf("%d\n",*iptr);
	printf("%d\n",*cptr);

	*iptr = y;
	*cptr = x;

	printf("After Swaping: \n");
	printf("%d\n",*iptr);
	printf("%d\n",*cptr);


}