/*9. take no of rows from the user
      1
    1 2
  1 2 3
1 2 3 4*/

#include <stdio.h>

void main(){

	int rows,num;

	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){
		num=1;
		for(int j=0; j<rows-i-1; j++ ){

			printf(" \t");
		}for(int k=0; k<i+1; k++){
			printf("%d\t",num);
			num++;
		}
		printf("\n");
	}
}