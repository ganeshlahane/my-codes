/*10. take no of rows from the user
D D D D
  C C C
    B B
      A*/

#include <stdio.h>

void main(){

	int rows,ch;

	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){
		for(int j=0; j<i; j++){

			printf(" \t");
		}
		ch=64+rows-i;
		for(int k=0; k<rows-i; k++){

			printf("%c\t",ch);
		}printf("\n");
	}
}