/*6. take no of rows from the user
D e F g
e D c B
F g H i
g F e D*/

#include <stdio.h>

void main(){

	int rows,ch;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){
		ch=64+rows+i;
		for(int j=0; j<rows; j++){

			if(i%2==0)
			{

				if(j%2==0)
					printf(" %c ",ch);
				else
					printf(" %c ",ch+32);
				ch++;
			}
			else
			{
				if(j%2==1)
					printf(" %c ",ch);
				else
					printf(" %c ",ch+32);
				ch--;
			}
		}printf("\n");
	}
}