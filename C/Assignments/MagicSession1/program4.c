/*4. take no of rows from the user
* - - -
- * - -
- - * -
- - - *  */

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			if(j==i){
				printf("*\t");
			}else{
				printf("-\t");
			}
		}printf("\n");
	}
}