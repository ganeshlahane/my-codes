// 5. Write a program to print ASCII values from 0 to 127.

#include <stdio.h>

void main(){

	int num1;
	int num2;

	printf("Enter First Number: ");
	scanf("%d",&num1);
	printf("Enter Second Number: ");
	scanf("%d",&num2);

	for(int i=num1; i<=num2; i++){
		printf("%c = %d\n",i,i);
	}

}
