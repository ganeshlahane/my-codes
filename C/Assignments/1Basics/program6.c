//6. Write a program to print reverse from 100-1.

#include <stdio.h>

void main(){

	int num1;
	int num2;

	printf("Enter First Number: ");
	scanf("%d",&num1);
	printf("Enter Second Number: ");
	scanf("%d",&num2);

	for(int i=num2; i>=num1; i--){
		printf("%d\n",i);
	}

}
