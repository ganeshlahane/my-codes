//10. Write a program to print the product of the first 10 numbers.

#include <stdio.h>

void main(){

	int num1,num2,product=1,count=0;

	printf("Enter First Number: ");
	scanf("%d",&num1);
	printf("Enter Second Number: ");
	scanf("%d",&num2);

	for(int i=num1; i<=num2; i++){
		
		product = product*i;
		count++;
		if(count==10){
			break;
		}
	}printf("%d\n",product);
}
