//8. Write a program to print a table of 11 in reverse order.

#include <stdio.h>

void main(){
	
	int num;
	int table;

	printf("Enter Number: ");
	scanf("%d",&num);

	for(int i=10; i>=1; i--){
		table = num*i;
		printf("%d\n",table);
	}
}
