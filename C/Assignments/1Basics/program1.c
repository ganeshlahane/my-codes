//1. Write a program to print the first 10 capital Alphabets.

#include <stdio.h>

void main(){

	char chr;
	printf("Enter character: ");
	scanf("%c",&chr);

	for(char i=chr; i<chr+10; i++){
		printf("%c\n",i);
	}
}
