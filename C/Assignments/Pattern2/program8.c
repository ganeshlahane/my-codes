/*8. take no of rows from the user
18 16 14
12 10 8
6  4  2
*/

#include <stdio.h>

void main(){

	int num=18,rows;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			printf("%d ",num);
			num-=2;
		}printf("\n");
	}
}
