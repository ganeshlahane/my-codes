/*5. take no of rows from the user
D C B A
e d c b
F E D C
g f e d

*/

#include <stdio.h>

void main(){

	int rows,ch;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);
	//ch=64+rows;
	for(int i=0; i<rows; i++){
		ch=64+rows+i;
		for(int j=0; j<rows; j++){
			
			if(i%2==0){
				printf("%c ",ch);
				ch--;
			}else{
				printf("%c ",ch+32);
				ch--;
			}
					
		}
		printf("\n");
	}
}
