/*7. take no of rows from the user
1   2   3   4
25  36  49  64
9   10  11  12
169 196 225 256
*/

#include <stdio.h>

void main(){

	int num=1,rows;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			if(i%2==1){
				printf("%d ",num*num);
				num++;
				
			}else{
				printf("%d ",num);
				num++;
			}
		}
		printf("\n");
	}
}
