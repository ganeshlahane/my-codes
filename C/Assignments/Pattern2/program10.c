/*10. take no of rows from the user
D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
A1 B2 C3 D4
*/

#include <stdio.h>

void main(){

	int rows,cols,ch,num;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);
	printf("Enter no. of cols: ");
	scanf("%d",&cols);
	for(int i=0; i<rows; i++){
		if(i%2==0){
			ch=64+cols;
			num=cols;
		}else{
			ch=65;
			num=1;
		}
		for(int j=0; j<cols; j++){

			if(i%2==0){
				printf("%c%d ",ch,num);
				ch--,num--;
			}else{
				printf("%c%d ",ch,num);
				ch++,num++;
			}
		}printf("\n");
	}
}
