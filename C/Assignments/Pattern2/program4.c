/*4. take no of rows from the user
I H G
F E D
C B A

*/

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);
	char ch = 'I';

	for(int i=1; i<=rows; i++){

		for(int j=1; j<=rows; j++){

			printf("%c ",ch);
			ch--;
		}printf("\n");
		
	}
}
