/*9. take no of rows from the user
0   1   1   2 
3   5   8   13
21  34  55  89
144 233 377 610  */

#include <stdio.h>

void main(){

	int rows,temp=0,num=0,x=1;

	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			temp=num;
			printf(" %d ",num);
			num=num+x;
			x=temp;
		}
		printf("\n");
	}
}