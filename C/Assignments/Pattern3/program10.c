/*10. take no of rows from the user        Can't solve the above try both separately
                                                     1 2 3 4
                                                     5 4 3 2
                                                     3 4 5 6
                                                     7 6 5 4
                                                    
                                                     D C B A
                                                     e f g h
                                                     F E D C
                                                     g h i j
                                                   Now mix the both codes
D1 C2 B3 A4
e5 f4 g3 h2
F3 E4 D5 C6
g7 h6 i5 j4
*/

#include <stdio.h>

void main(){

	int rows,ch,num=1;

	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			if(i%2==0){
				printf(" %d ",num);
				num++;
			}else{
				printf(" %d ",num);
				num--;
			}

		}
		printf("\n");
	}
}