/*4. take no of rows from the user
a B c D
b C d E
c D e F
d E f G
*/

#include <stdio.h>

void main(){

	int rows,ch=97;

	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){
		ch=96+i;
		for(int j=1; j<=rows; j++){
			if(j%2==1){
				printf(" %c ",ch);
				ch++;	
			}else{
				printf(" %c ",ch-32);
				ch++;
			}	
		}printf("\n");
			
	}
}