/*6. take no of rows from the user
= = = = = =
$ $ $ $ $ $
@ @ @ @ @ @
= = = = = =
$ $ $ $ $ $
@ @ @ @ @ @
*/

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no. of  rows: ");
	scanf("%d",&rows);

	for(int i=1; i<=rows; i++){

		for(int j=1; j<=rows; j++){

			if(i%2==1){
				printf(" = ");
			}else if(i%2==0){
				printf(" $ ");
			}else{
				printf(" @ ");
			}
		}printf("\n");
	}
}