/*8. take no of rows from the user
16 15 14 13
L  K  J  I
8  7  6  5
D  C  B  A
*/

#include <stdio.h>

void main(){

	int rows,num=16,ch='P';

	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows; j++){

			if(i%2==0){
				printf(" %d ",num);
			}else{
				printf(" %c ",ch);
			}
			ch--;
			num--;
		}printf("\n");
	}
}