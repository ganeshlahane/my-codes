/* Program 9: 
Write a program, take a number and print whether it is positive or negative. Take all the values from the user
Input: var=5
Output: 5 is a positive number

Input: var=-9
Output: -9 is a negative number
*/
#include <stdio.h>

void main(){

	int x;
	printf("Enter number: ");
	scanf("%d",&x);

	if(x>0){
		printf("%d is a positive number\n",x);
	}else{
		printf("%d is a negetive number\n",x);
	}
}
