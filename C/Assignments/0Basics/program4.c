/*Program 4:
WAP to find min among 2 numbers. Take all the values from the user
Input : 2 3
Output: 2
*/


#include <stdio.h>

void main(){

	int num1;
	int num2;

	printf("Enter First number: ");
	scanf("%d",&num1);

	printf("Enter Second number: ");
	scanf("%d",&num2);

	if(num1<num2){
		printf("%d is Minimum\n",num1);
	}else{
		printf("%d is Minimum\n",num2);
	}
}
