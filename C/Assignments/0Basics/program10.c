/* Program 10: 
Write a program to check if a character is a vowel or consonant. Take all the values from the user
Input: var= ”A”;
Output: A is a vowel.

Input: var= ”D”;
Output: D is a consonant.
*/
#include <stdio.h>

void main(){
	char var;

	printf("Enter character: ");
	scanf("%c",&var);

	if(var>=65 && var<=90 || var>=97 && var<=122){

		if(var=='A'||var=='E'||var=='I'||var=='O'||var=='U' || var=='a'||var=='e'||var=='i'||var=='o'||var=='u'){

			printf("%c is a vowel\n",var);
		}else{
			printf("%c is a consonant\n",var);
		}
	}else{
		printf("Invalid Input\n");
	}
}
