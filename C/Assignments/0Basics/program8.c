/* Program 8: 
Write a program, take a character and print whether it is in
UPPERCASE or lowercase. Take all the values from the user

Input: var = A
Output: You entered the UPPERCASE character.
*/

#include <stdio.h>

void main(){

	char var;
	
	printf("Enter character: ");
	scanf("%c",&var);

	if(var>=65 && var<=90){
		printf("You entered the UPPERCASE character\n");
	}else if (var>=97 && var<=122){
		printf("You entered the LOWERCASE character\n");
	}else{
		printf("Invalid input\n");
	}

}
