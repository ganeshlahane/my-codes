/*Program 3:
WAP to find max among 2 numbers. Take all the values from the user.
Input : 2 4
Output: 4   
*/

#include <stdio.h>

void main(){

	int num1;
	int num2;

	printf("Enter First number: ");
	scanf("%d",&num1);

	printf("Enter Second number: ");
	scanf("%d",&num2);

	if(num1>num2){
		printf("%d is greater\n",num1);
	}else{
		printf("%d is greater\n",num2);
	}
}
