/*
	1
	1 2
	2 3 4
	4 5 6 7
	7 8 9 10 11
*/

#include <stdio.h>

void main(){

	int rows,num=1;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<i+1; j++){

			printf("%d ",num);
			num++;
		}num--;
		printf("\n");
	}
}