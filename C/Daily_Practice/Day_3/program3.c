/*
	a
	b B
	c C c
	d D d D
*/

#include <stdio.h>

void main(){

	int rows,ch;

	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){
		ch = 65+i;
		for(int j=0; j<i+1; j++){

			if(j%2==0){
				printf("%c\t",ch+32);
			}else{
				printf("%c\t",ch);
			}
		}printf("\n");
	}
}