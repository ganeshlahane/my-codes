/* 
	2 
	3 4
	4 5 6
	5 6 7 8
*/

#include <stdio.h>

void main(){

	int row;

	printf("Enter No. of Rows: ");
	scanf("%d",&row);

	for(int i=0; i<row; i++){
		int num = i+2;
		for(int j=0; j<i+1; j++){

			printf("%d ",num);
			num+=1;
		}
		printf("\n");
	}
}