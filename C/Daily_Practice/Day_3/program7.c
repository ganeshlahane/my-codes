// Write a program to print Given number is Harshad number or not.

#include <stdio.h>

void main(){

	int num,sum=0,rem=0;
	printf("Enter Number: ");
	scanf("%d",&num);
	int temp = num;
	
	while(num!=0){

		rem = num % 10;
		sum = sum + rem;
		num = num/10;
	}
	if(temp%sum==0){

		printf("%d is a Harshad Number\n",temp);
	}else{
		printf("%d is not a Harshad Number\n",temp);
	}
}