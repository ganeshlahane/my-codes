/*
	5 
	5 d
	5 d 4
	5 d 4 b
	5 d 4 b 3
*/

#include <stdio.h>

void main(){

	int r;
	printf("Enter no. of rows: ");
	scanf("%d",&r);

	for(int i=0; i<r; i++){
		int n=r, ch=64+r;
		for(int j=0; j<i+1; j++){

			if(j%2==0){
				printf("%d ",n);
				n--;
			}else{
				printf("%c ",ch+32);
			}ch--;
		}printf("\n");
	}
}
