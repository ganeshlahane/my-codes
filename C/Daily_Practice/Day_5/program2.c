/*
	0  1  1 2 
	3  5  8        (Fibonacci Number pattern)
	13 21 
	34
*/

#include <stdio.h>

void main(){

	int rows,temp=0,x=1,num=0;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<rows-i; j++){

			temp = num;
			printf("%d\t",num);
			num = num+x;
			x = temp;
		}printf("\n");
	}

}