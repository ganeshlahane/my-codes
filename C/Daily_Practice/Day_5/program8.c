// WAP to print perfect number in given range.

#include <stdio.h>

void main(){

	int Snum,Enum;
	printf("Enter Start Number: ");
	scanf("%d",&Snum);
	printf("Enter End Number: ");
	scanf("%d",&Enum);

	for(int i=Snum; i<Enum; i++){
		int sum=0;
		for(int j=1; j<i; j++){

			if(i%j==0){

				sum = sum + j;
			}
		}
		if(i==sum){
			printf("%d\n",i);
		}
	}
}