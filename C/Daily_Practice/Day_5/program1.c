/*
	A B C D
	  A B C
	    A B
	      A
*/

#include <stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){

		for(int j=0; j<i; j++){

			printf("  ");
		}
		int ch = 65;
		for(int k=rows; k>i; k--){

			printf("%c ",ch);
			ch++;
		}printf("\n");
	}
}