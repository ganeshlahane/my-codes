// WAP to print given number is perfect or not.

#include <stdio.h>

void main(){

	int num,sum=0;
	printf("Enter Number: ");
	scanf("%d",&num);

	for(int i=1; i<num; i++){

		if(num%i==0){

			sum = sum+i;
		}
	}
	if(num==sum){

		printf("%d is a Perfect Number\n",num);
	}else{
		printf("%d is not a Perfect Number\n",num);
	}
}