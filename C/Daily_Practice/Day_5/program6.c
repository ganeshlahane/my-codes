// WAP to print count of digits of given number.

#include <stdio.h>

void main(){

	int num,count=0,rem=0,temp=0;
	printf("Enter Number: ");
	scanf("%d",&num);
	temp = num;
	while(num!=0){

		rem = num;
		count++;
		num = num/10;
	}
	printf("digit count of %d is %d\n",temp,count);
} 