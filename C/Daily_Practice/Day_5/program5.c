/*
	a
	a b
	a b c
	a b c d
	a b c
	a b
	a
*/

#include <stdio.h>

void main(){

	int r;
	printf("Enter no. of rows: ");
	scanf("%d",&r);
	for(int i=0; i<r; i++){
		int ch=97;
		if(i<(r/2)){

			for(int j=0; j<i+1; j++){
				printf("%c ",ch);
				ch++;
			}printf("\n");
		}else{
			for(int k=0; k<r-i; k++){
				printf("%c ",ch);
				ch++;
			}printf("\n");

		}
	}

}