/*
	3 d 2 c
	  d 2 c
	    1 d
	      d
*/

#include <stdio.h>

void main(){

	int rows,n=1;
	printf("Enetr no. of rows: ");
	scanf("%d",&rows);

	for(int i=0; i<rows; i++){
		int n=rows-i-1;
		char ch = 'a'+rows-1;
		for(int j=0 ; j<rows ; j++ ){
			if(j<i){
				printf("   ");
				continue;
			}
			if(j%2==0){
				printf(" %d ",n--);
			}else{
				printf(" %c ",ch--);
			}
		}
		
		printf("\n");
	}
		
}