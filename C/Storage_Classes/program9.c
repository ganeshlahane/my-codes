//Accessing 'x' variable form another file.

#include <stdio.h>

void fun(){

	extern int x;     // undefined reference to 'x'
	printf("%d\n",x);
}