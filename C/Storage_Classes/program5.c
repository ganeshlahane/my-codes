// Senario of 'Local static variable'

#include <stdio.h>

void fun(){

	static int x = 10; // In this senario the location of 'x' variable is in the Data Section.

	++x;

	printf("%d\n",x);
}		
void main(){

	fun();
	fun();
	fun();
}