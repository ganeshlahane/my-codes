
#include <stdio.h>

int z;                  

void fun(int x,int y){

	auto int a;          
	register int b = 30;
	static int c = 40;
	//extern int d;     // Unfined reference to 'd'

	printf("%d\n",x);
	printf("%d\n",y);
	printf("%d\n",a);  // Garbage value
	printf("%d\n",b);  
	printf("%d\n",c);  
} 
void main(){

	fun(10,20);

	printf("%d\n",z);  // 0
}