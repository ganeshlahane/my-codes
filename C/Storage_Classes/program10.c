
#include <stdio.h>


void main(){

	extern int x;

	printf("%d\n",x);
}

static int x = 30; // static declaration of 'x' follows non-static declaration 

				   // In a same file we can't use static and extern on same variable.
 