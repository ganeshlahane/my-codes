// Senario-2 of 'Global static variable'

#include <stdio.h>

void fun();

static int x = 10;  // When we use static storage class then we cannot access that variable from another file.

void main(){

	fun();
}
