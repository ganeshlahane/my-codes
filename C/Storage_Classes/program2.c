#include <stdio.h>

register int x = 10; // error: register name not specified for 'x'

auto int y = 30; // error: file-scope declaration of 'y' specifies 'auto'

void main(){

	register int a = 20; 

	auto int b = 40;
						/*
								auto and register storage classes we can only apply for loacal variables
						*/
}