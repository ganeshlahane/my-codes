// Take an input number from the user and count the digits.

#include <stdio.h>

void main(){

	int num,rem,count=0;
	printf("Enter number: ");
	scanf("%d",&num);
	rem=num;

	while(num!=0){
		count++;
		num=num/10;

	}printf("Total no. of count in %d is %d\n",rem,count);
}