/* 
take a number from the user and print the fibonacci series upto that number.

 input: 10
 output: 0 1 1 2 3 5 8
 */

#include <stdio.h>

void main(){

	int num,x=0,y=1,temp;
	printf("Enter Number: ");
	scanf("%d",&num);

	while(x<num){
		temp=x;
		printf("%d ",x);
		x=x+y;
		y=temp;
	}
	printf("\n");
}