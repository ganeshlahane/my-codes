// Take the input from the user and print the product of digits.

#include <stdio.h>

void main(){

	int num,rem,mult=1;
	printf("Enter number: ");
	scanf("%d",&num);

	while(num!=0){
		rem = num%10;
		mult =mult*rem;
		num=num/10;

	}printf("product of given number digits is = %d\n",mult);
}