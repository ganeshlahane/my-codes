/* WAP to find the sum of numbers that are not divisible by 3 upto given number.
input : 10
output : sum of number that are not divisible by 3 is 37.
*/
#include <stdio.h>

void main(){

	int num,add=0;
	printf("Enter Number: ");
	scanf("%d",&num);

	for(int i=1; i<=num; i++){

		if(i%3!=0){
			add = add+i;
		}
	}		printf("sum of numbers that are not divisible by 3 is %d\n",add);
} 
