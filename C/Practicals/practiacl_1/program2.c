// WAP to find max among 3 numbers.

#include <stdio.h>

void main(){

	int x,y,z;

	printf("Enter First Number: ");
	scanf("%d",&x);
	printf("Enter Second Number: ");
	scanf("%d",&y);
	printf("Enter Third Number: ");
	scanf("%d",&z);

	if(x>y && x>z ){
		printf("%d is greater than %d and %d\n",x,y,z);
	}else if(y>x && y>z){
		printf("%d is greater than %d and %d\n",y,x,z);
	}else if(z>x && z>y){
		printf("%d is greater than %d and %d\n",z,x,y);
	}

}
