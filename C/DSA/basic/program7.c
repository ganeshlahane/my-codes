#include <stdio.h>
#include <stdlib.h>

typedef struct Student {

	int Id;
	float Ht;
	struct Student *next;
}Stud;

void main(){

	Stud *head = NULL;

	Stud *newNode = (Stud*)malloc(sizeof(Stud));
	newNode->Id = 10;
	newNode->Ht = 5.5;
	newNode->next = NULL;

	head = newNode;

	newNode = (Stud*)malloc(sizeof(Stud));
	newNode->Id = 20;
	newNode->Ht = 6.5;
	newNode->next = NULL;

	head->next = newNode;

	newNode = (Stud*)malloc(sizeof(Stud));
	newNode->Id = 30;
	newNode->Ht = 7.5;
	newNode->next = NULL;

	head->next->next = newNode;

	Stud *temp = head;
	while(temp != NULL){

		printf("|%d ",temp->Id);
		printf("%f| ",temp->Ht);
		temp = temp->next;
	}
	printf("\n");
}