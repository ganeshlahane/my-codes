#include <stdio.h>
#include <stdlib.h>

typedef struct Forts {

	char fName[20];
	int noOfDoors;
	struct Forts *next;
}Forts;

Forts *head = NULL;
Forts* createNode(){

	Forts *newNode = (Forts*)malloc(sizeof(Forts));
	getchar();
	printf("Enter Name of Fort:: ");
	gets(newNode->fName);
	printf("Enter no. of Doors:: ");
	scanf("%d",&newNode->noOfDoors);
	newNode->next = NULL;

	return newNode;
}
void addNode(){

	Forts *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
		Forts *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}
void addFirst(){

	Forts *newNode = createNode();
	if(head == NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head = newNode;
	}
}
void addAtPosition(int pos){

	Forts *newNode = createNode();
	Forts *temp = head;
	while(pos-2){
		temp = temp->next;
		pos--;
	}
	newNode->next = temp->next;
	temp->next = newNode;
}
void printLL(){

	Forts *temp = head;
	while(temp != NULL){
		printf("|%s->",temp->fName);
		printf("%d|",temp->noOfDoors);
		temp = temp->next;
	}
	printf("\n");
}
int nodeCount(){
	int count = 0;
	Forts *temp = head;
	while(temp->next != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}
void deleteFirst(){
	Forts *temp = head;
	head = temp->next;
	free(temp);
}
void deleteLast(){
	Forts *temp = head;
	while(temp->next->next != NULL){
		temp = temp->next;
	}
	free(temp->next);
	temp->next = NULL;
}
void main(){

	char choise;
	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPosition\n");
		printf("4.printLL\n");
		printf("5.deleteFirst\n");
		printf("6.deleteLast\n");
		printf("7.nodeCount\n");
		int ch;
		printf("Enter Number:: ");
		scanf("%d",&ch);

		switch(ch){

		case 1:
			addNode();
			break;
		case 2:
			addFirst();
			break;
		case 3:
			{
			int pos;
			printf("Enter position for add a node:: ");
			scanf("%d",&pos);
			int count = nodeCount();
			if(pos >= count){
				addAtPosition(pos);
			}else{
				printf("Wrong position \n");
			}
		}
			break;
		case 4:
			printLL();
			break;
		case 5:
			deleteFirst();
			break;
		case 6:
			deleteLast();
			break;
		case 7:
			{
				int count = nodeCount();
				printf("Total Number of nodes is = %d\n",count);
			}	
			break;
		default:
			printf("Wrong choise\n");
		}
		getchar();
		printf("Do you want to continue:: ");
		scanf("%c",&choise);
	}while(choise == 'y' || choise == 'Y');	
}