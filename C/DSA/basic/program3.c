#include <stdio.h>
#include <string.h>

struct Company {

	char Cname[20];
	int empCount;
	float Revenue;
	struct Company *next;
};
void main(){

	struct Company obj1,obj2,obj3;

	struct Company *head = &obj1; 

	strcpy(head->Cname,"Alphabet");
	head->empCount = 500;
	head->Revenue = 50.4;
	head->next = &obj2;

	strcpy(head->next->Cname,"Google");
	head->next->empCount = 1000;
	head->next->Revenue = 90.5;
	head->next->next = &obj3;

	strcpy(head->next->next->Cname,"Android");
	head->next->next->empCount = 700;
	head->next->next->Revenue = 70.6;
	head->next->next->next = NULL;

	puts(head->Cname);
	printf("%d\n",head->empCount);
	printf("%f\n",head->Revenue);

	puts(head->next->Cname);
	printf("%d\n",head->next->empCount);
	printf("%f\n",head->next->Revenue);

	printf("%s\n",head->next->next->Cname);
	printf("%d\n",head->next->next->empCount);
	printf("%f\n",head->next->next->Revenue);
}