#include <stdio.h>
#include <stdlib.h>

typedef struct Student {

	int Id;
	float Ht;
	struct Student *next; 
}Stud;

Stud *head = NULL; // head pointer globally decleared

void addNode(){

	Stud *newNode = (Stud*)malloc(sizeof(Stud));
	newNode->Id = 10;
	newNode->Ht = 5.5;
	newNode->next = NULL;

	head = newNode;
}

void printData(){

	Stud *temp = head;

	while(temp != NULL){

		printf("%d ",temp->Id);
		printf("%f ",temp->Ht);
		temp = temp->next;
	}
	printf("\n");
}
void main(){

	addNode();
	printData();
}