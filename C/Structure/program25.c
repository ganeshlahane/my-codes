// Enum or Enumeration

#include <stdio.h>

enum Friends {

	Ganesh,
	Karan,
	Mangesh,
	Prasad,
	Aatish,
	Abhay,
};

void main(){

	enum Friends obj;

	printf("%ld\n",sizeof(obj));

	// Accessing Enum elements.

	printf("%d\n",Ganesh);
	printf("%d\n",Karan);
	printf("%d\n",Mangesh);
	printf("%d\n",Prasad);
	printf("%d\n",Aatish);
	printf("%d\n",Abhay);
}