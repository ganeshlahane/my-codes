// Real time example by using mystrcpy

#include <stdio.h>

struct Trekking {

	char location[20];
	int members;
	float distance;
};

void* mystrcpy(char* dest, char* source){

	while(*source != '\0'){

		*dest = *source;
		dest++;
		source++;
	}
	*dest = '\0';
	return dest;
}

void main(){

	struct Trekking Fort1 = {"Visapur",5,60};

	struct Trekking Fort2;

	mystrcpy(Fort2.location,"Raigad");
	Fort2.members =  7;
	Fort2.distance = 130;

	puts(Fort1.location);
	printf("%d\n",Fort1.members);
	printf("%f\n",Fort1.distance);

	puts(Fort2.location);
	printf("%d\n",Fort2.members);
	printf("%f\n",Fort2.distance);
}