// Nested Structure

#include <stdio.h>
#include <string.h>

struct movieInfo {

	char actor[10];
	float imdbRate;
};

struct Movie {

	char mName[10];

	struct movieInfo obj1;
};

void main(){

	struct Movie obj2 = {"Pushpa",{"AlluArjun",8.5}};  // Second way to give an input

/*
	strcpy(obj2.mName,"Pushpa");
	strcpy(obj2.obj1.actor,"AlluArjun");   // First way to give an input
	obj2.obj1.imdbRate = 8.5;
*/

	printf("%s\n",obj2.mName);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.imdbRate);
}