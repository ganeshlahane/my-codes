

#include <stdio.h>

struct Emp {

	int x:4;
	float y;
	double z;
}obj1;

void main() {

	printf("%p\n",&obj1.x); // error can not take address of bit-feild 'x'
	printf("%p\n",&obj1.y);
	printf("%p\n",&obj1.z);
}