// Real time example.

#include <stdio.h>
#include <string.h>

struct Picnic {

	char location[20];
	int members;
	float distance;
};

void main(){

	struct Picnic obj1 = {"Goa",5,450.60};

	struct Picnic obj2;

	strcpy(obj2.location,"Kashmir");
	obj2.members =  2;
	obj2.distance = 1200;

	puts(obj1.location);
	printf("%d\n",obj1.members);
	printf("%f\n",obj1.distance);

	puts(obj2.location);
	printf("%d\n",obj2.members);
	printf("%f\n",obj2.distance);
}