
#include <stdio.h>

struct Demo {

	int x;
	float y;
};

void main(){

	struct Demo obj = {10,20.5};

	int arr[] = {10,20,30};

	printf("%p\n",&arr[0]);
	printf("%p\n",arr);

	printf("%p\n",&obj.x);
	printf("%p\n",&obj);
}