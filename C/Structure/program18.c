
#include <stdio.h>
#pragma pack(1)         // 5
struct Demo {

	int x:4;
	float y;
};

void main(){

	struct Demo obj = {15,50};

	printf("%ld\n",sizeof(obj)); // 8
}
