// Use of "typedef".

#include <stdio.h>

typedef struct Employee {   // typedef is use to give another name to structure

	char empName[20];
	int empId;
	float empSal;
}Emp;

void main(){

	struct Employee obj1 = {"Ganesh",295,10.50};

	// OR

	Emp obj2 = {"Karan",497,12.50};

	puts(obj1.empName);
	printf("%d\n",obj1.empId);
	printf("%f\n",obj1.empSal);
	
	puts(obj2.empName);
	printf("%d\n",obj2.empId);
	printf("%f\n",obj2.empSal);

}