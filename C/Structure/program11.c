// Pointer to a Structure or Structure Pointer.

#include <stdio.h>

struct Movie {

	char mName[10];  
	int members;
	float price;

}obj1 ={"Tumbbad",5,1000};

void main(){

	struct Movie *sptr = &obj1;

	printf("%s\n",obj1.mName);
	printf("%d\n",obj1.members);
	printf("%f\n",obj1.price);

	printf("%s\n",sptr->mName);
	printf("%d\n",sptr->members);
	printf("%f\n",sptr->price);
}