// Object initialization using malloc

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct IPL {

	char sName[10];  // Sponsor name
	int tTeams;   // Total teams
	double prize; // In Cr.   
};

void main(){

	struct IPL *ptr = (struct IPL*)malloc(sizeof(struct IPL));

	strcpy((*ptr).sName,"TATA");
	(*ptr).tTeams = 8;             // or ptr -> tTeams;
	(*ptr).prize = 10;             // or ptr -> prize;

	printf("%s\n",(*ptr).sName);
	printf("%d\n",(*ptr).tTeams);
	printf("%f\n",(*ptr).prize);
}