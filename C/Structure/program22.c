
#include <stdio.h>

union Employee {

	int empId;
	float sal;
};

void main(){

	union Employee emp1 = {10,50.60}; //not proper way to giving data

	// proper way

	union Employee emp2 ;

	emp2.empId = 15;

	printf("%d\n",emp2.empId);

	emp2.sal = 70.50;

	printf("%f\n",emp2.sal);
}