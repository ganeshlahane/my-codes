// Bit field.

#include <stdio.h>

struct Demo {

	char ch;
	int x:1;    // it gives bit wise memory or minimum 1 byte memory
	float y;
};

void main(){

	printf("%ld\n",sizeof(struct Demo)); // 8
}