//calloc(): It gives continues memory on heap section.

#include <stdio.h>
#include <stdlib.h>

void main(){

	int *ptr = (int*)calloc(5,sizeof(int));

	for(int i=0; i<5; i++){

		*(ptr+i) = 11+i;
	}
	for(int i=0; i<5; i++){

		printf("%d\n",*(ptr+i));
	}
}