// Dangling pointer Senario-2;

#include <stdio.h>
#include <stdlib.h>

void num(int x){
	
	int *ptr1 = (int*)malloc(sizeof(int));
                  
	*ptr1 = x;

	int *ptr2 = ptr1;

	printf("%d\n",*ptr1);
	printf("%d\n",*ptr2);

	free(ptr1);

	printf("%d\n",*ptr2);
}

void main(){

	num(10);
}