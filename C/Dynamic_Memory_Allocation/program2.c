// malloc: It gives memory on heap section.

#include <stdio.h>
#include <stdlib.h>

void fun(){

	int x = 10;

	int *ptr = (int*)malloc(sizeof(int));

	printf("%p\n",ptr);  // address
	printf("%p\n",*ptr); // (nil)
	printf("%d\n",*ptr); // GV

	*ptr = 50;

	printf("%p\n",ptr) ; // address 
	printf("%d\n",*ptr) ; // 50;
}
void main()
{
	fun();
}