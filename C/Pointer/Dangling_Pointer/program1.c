#include <stdio.h>

int a = 10;
int b;
int *ptr = 0;

void fun(){

	int x = 100;

	printf("%d\n",a); //10
	printf("%d\n",b); //0
	printf("\n");

	ptr = &x;

	printf("%p\n",a); 
	printf("%p\n",b); 
	printf("%p\n",ptr); 
	printf("%d\n",*ptr);
	printf("\n");

}

void main(){

	int y = 40;

	printf("%d\n",a); //10
	printf("%d\n",b); //0
	printf("%d\n",y); //40
	printf("\n");

	fun();

	printf("%p\n",ptr);
	printf("%d\n",*ptr);

}