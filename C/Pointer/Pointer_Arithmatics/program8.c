#include <stdio.h>

void main(){

	char ch1 = 'x';
	char ch2 = 'y';

	char *ptr = &ch1;

	printf("%p\n",ptr);
	printf("%c\n",*ptr);

	printf("%p\n",(ptr+1));
	printf("%c\n",*(ptr+1.5)); // While doing a addition of pointer it takes only integer value not a float. 

}