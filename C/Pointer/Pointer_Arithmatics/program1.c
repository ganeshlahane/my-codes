
#include <stdio.h>

void main(){

	int x = 10;
	
	int *ptr1=&x;
	int *ptr2=x;
	
	printf("%d\n",ptr1);  // -12345   
	
	printf("%p\n",ptr1);  // 0x full address
	
	printf("%p\n",ptr2);  // 0xa

	printf("%d\n",*ptr1); // 10 
	
	printf("%p\n",*ptr1); // 0xa
		
	printf("%d\n",*ptr2); // segmentation fault
}