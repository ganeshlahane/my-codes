//if entered number is even then print reverse that number upto 1 in single intervel gap and 
//if entered number is odd  then print reverse that number upto 1 in double intervel gap.

#include <stdio.h>

void main(){

	int num;
	printf("Enter Number: ");
	scanf("%d",&num);

	if(num%2==0){

		while(num>0){
			printf("%d\n",num);
			num--;
		}
	}else{
		while(num>0){
			printf("%d\n",num);
			num-=2;
		}
	}
}