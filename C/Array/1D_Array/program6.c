// Searching array elements

#include <stdio.h>

void main(){

	int size,search=0,flag=0;

	printf("Enter array size: ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements: \n");

	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}

	printf("array elements are: \n");

	for(int i=0; i<size; i++){

		printf("%d\n",arr[i]);
	}

	printf("Enter search element: ");
	scanf("%d",&search);

	for(int i=0; i<size; i++){

		if(search==arr[i]){

			flag=1;
		}
	}
	if(flag==1){
		printf("Element found\n");
	}else{
		printf("Element are not found\n");
	}
}