// assigning array elements.

#include <stdio.h>
/*
void main(){

	char arr[4];

	arr[0]='A';
	arr[1]='B';
	arr[2]='C';
	arr[3]='D';

	printf("%c\n",arr[0]);
	printf("%c\n",arr[1]);
	printf("%c\n",arr[2]);
	printf("%c\n",arr[3]);
}
*/

// OR

void main(){

	int arr[5];

	printf("Enter Array elements: \n");

	for(int i=0; i<5; i++){

		scanf("%d",&arr[i]);
	}
	
	printf("Print Array elements: \n");

	for(int i=0; i<5; i++){

		printf("%d\n",arr[i]);
	}
}