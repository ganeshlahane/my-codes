// Assigning array to an array.

#include <stdio.h>

void main(){

	int size,add=0;

	printf("Eter array Size: ");
	scanf("%d",&size);

	int arr1[size];

	printf("Enter array elements: \n");

	for(int i=0; i<size; i++){

		scanf("%d",&arr1[i]);
	}
	printf("arr1 elements are: \n");
	for(int i=0; i<size; i++){

		printf("%d\n",arr1[i]);
	} 

	int arr2[size];

	for(int i=0; i<size; i++){

		arr2[i]=arr1[i];
	}
	printf("arr2 elements are: \n");
	for(int i=0; i<size; i++){

		printf("%d\n",arr2[i]);
	}
}