// print count of those numbers that are divisible by 2.

#include <stdio.h>

void main(){

	int size,count=0;

	printf("Enter Array size: ");
	scanf("%d",&size);

	int arr[size];
	printf("Enter Array Elements: \n");

	for(int i=0; i<size; i++){

		scanf("%d",&arr[i]);
	}
	
	for(int i=0; i<size; i++){

		if(i%2==0){
			printf("%d divisible by 2\n",i);
			count++;
		}
	}
	printf("%d numbers are divisible by 2 in Array\n",count);
}