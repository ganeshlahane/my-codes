#include <stdio.h>

void main(){

	int arr1[] = {1,2,3,4,5,6,7,8,9};

	int arr2[3][3] = {1,2,3,4,5,6,7,8,9};

	/*
		 1 2 3
		 4 5 6
		 7 8 9	
	*/

	int arr3[][3] = {1,2,3,4,5,6,7,8,9};

	/*
		1 2 3 
		4 5 6
		7 8 9
	*/

	int arr4[2][3] = {1,2,3,4};

	/*
		1 2 3
		4 0 0
	*/

	//int arr[3][] = {1,2,3,4,5,6,7,8,9,};  // error: array type has incomplete element type 'int[]'

	//printf("%d\n",arr1[]);
	printf("%d\n",arr2[]);
	//printf("%d\n",arr3[][3]);
	printf("%d\n",arr4);
}