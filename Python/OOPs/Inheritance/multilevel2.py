class Parent(object):

	x = 10

	def __init__(self):
		print("Parent Constructer")
		self.y = 20

	@classmethod
	def show(cls,n):
		print(n)

	def disp(self,n):
		print(n)
		print(self.y)

class Child(Parent):

	x = 295

	def __init__(self):
		super().__init__()
		print("Child Constructer")

	def chldDisp(self):
		super().show(super().x)
		print(self.x)
		print(self.y)

obj = Child()
obj.chldDisp()
obj.show()

