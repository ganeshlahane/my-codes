class gf:
	def __init__(self):
		print('GF CONS')
		self.g=10
	def gfdisp(self):
		print('Grand fater property :',self.g)
class f(gf):
	def __init__(self):
		super().__init__()
		print('F CONS')
		self.f=20
	def fdisp(self):
		print('Fater property :',self.f)
class c(f):
	def __init__(self):
		super().__init__()
		print('C CONS')
		self.c=30
	def cdisp(self):
		print('Child property :',self.c)
'''
objc=c()
objc.cdisp()
objc.fdisp()
objc.gfdisp()

objf=f()
objf.fdisp()
objf.gfdisp()
objf.cdisp()
'''
objgf=gf()
objgf.gfdisp()
objgf.fdisp()
objgf.cdisp()