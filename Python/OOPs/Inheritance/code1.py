class Parent:
	
	def __init__(self):
		print("Parent Constructor")
		self.x = 10

	def disp(self):
		print(self.x)

class Child(Parent):
	
	def __init__(self):
		print("Child Constructor")
		super().__init__()
		self.y = 20
		self.disp(+)

	def show(self):
		#self.disp()
		print(self.y)

obj = Child()
obj.show()
