class Company:

	def __init__(self,Cname,NoofEmployees):
		self.Cname = Cname
		self.NoofEmployees = NoofEmployees

	def CompInfo(self):
		print(self.Cname)
		print(self.NoofEmployees)

class Employee(Company):

	def __init__(self,empName,empId,Cname,NoofEmployees):
		super().__init__(Cname,NoofEmployees)
		self.empName = empName
		self.empId = empId
		#self.CompInfo()

	def EmpInfo(self):
		print(self.empName)
		print(self.empId)
		self.CompInfo()

empName = input("Enter Employee Name: ")
empId = int(input("Enter Employee Id: "))
Cname = input("Enter Company Name: ")
NoofEmployees = int(input("Enter total no.of Employees: "))

obj = Employee(empName,empId,Cname,NoofEmployees)

obj.EmpInfo()                                           