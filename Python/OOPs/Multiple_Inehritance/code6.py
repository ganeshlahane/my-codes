class X:

	def __init__(self):
		super().__init__()
		print("X Constructer")

class Y:

	def __init__(self):
		super().__init__()
		print("Y Constructer")

class A(X):

	def __init__(self):
		super().__init__()
		print("A Constructer")

class B(X):

	def __init__(self):
		super().__init__()
		print("B Constructer")

class C(Y):

	def __init__(self):
		print("C Constructer")

class D(A,B,C):

	def __init__(self):
		super().__init__()
		print("D Constructer")

obj = D()
print(D.mro())