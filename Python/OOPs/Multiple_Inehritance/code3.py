class A:

	def __init__(self):
		super().__init__()
		print("A Constructer")

class B:

	def __init__(self):
		super().__init__()
		print("B Constructer")

class C:

	def __init__(self):
		print("C Constructer")

class D(A,B,C):

	def __init__(self):
		super().__init__()
		print("D Constructer")

obj = D()
print(D.mro())


