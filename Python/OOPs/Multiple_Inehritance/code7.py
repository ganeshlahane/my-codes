class X:

	def __init__(self):
		super().__init__()
		print("X Constructer")

class Y:

	def __init__(self):
		super().__init__()
		print("Y Constructer")

class Z:

	def __init__(self):
		super().__init__()
		print("Z Constructer")


class A(X):

	def __init__(self):
		super().__init__()
		print("A Constructer")

class B(Y):

	def __init__(self):
		super().__init__()
		print("B Constructer")

class C(Z):

	def __init__(self):
		super().__init__()
		print("C Constructer")

class D(A,B,C):

	def __init__(self):
		super().__init__()
		print("D Constructer")

obj = D()
print(D.mro())