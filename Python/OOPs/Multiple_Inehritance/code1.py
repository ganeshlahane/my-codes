class Parent1():

	def __init__(self):
		super().__init__()
		print("Parent1 Constructer")

class Parent2():
	
	def __init__(self):
		print("Parent2 Constructer")

class Child(Parent1,Parent2):

	def __init__(self):
		super().__init__()
		print("Child Constructer")

Obj = Child()
print(Child.mro())
