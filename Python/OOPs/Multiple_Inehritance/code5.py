class X:

	def __init__(self):
		super().__init__()
		print("X Constructer")

class A(X):

	def __init__(self):
		print("A Constructer")

class B(X):

	def __init__(self):
		super().__init__()
		print("B Constructer")

class C:

	def __init__(self):
		print("C Constructer")

class D(A,B,C):

	def __init__(self):
		super().__init__()
		print("D Constructer")

obj = D()
print(D.mro())