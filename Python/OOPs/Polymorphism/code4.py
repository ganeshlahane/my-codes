# Method Overriding

class Parent:

	x = 10 

	@classmethod
	def show(self):
		print(self.x)

class child(Parent):

	def __init__(self):
		self.x = 295

	def show(self):
		print(self.x)
		super().show()

obj = child()
obj.show()

