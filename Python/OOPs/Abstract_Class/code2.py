# metaclass concept

class xyz:
	pass

name = "Ganesh"
age = 20
sal = 25.00

def fun():
	pass

print(type(type(xyz)))
print(type(type(name)))
print(type(type(age)))
print(type(type(sal)))
print(type(type(fun)))

print(name.__class__)
print(xyz.__dict__)