class Outer:

	x = 10

	def __init__(self):
		print("outer Constructor")
		self.out = 20

	class Inner:
		y = 30
		def __init__(self):
			print(" Inner Constructor")
			self.inn = 40

		def dispInner(self):
			print(self.inn)
			print(self.y)
	@classmethod			
	def dispOuter(self):
		#print(self.out)
		print(Outer.x)

outObj = Outer()
outObj.dispOuter()

innObj1 = outObj.Inner()
innObj1.dispInner()

innObj2 = Outer().Inner()
innObj2.dispInner

