class Outer:

	def __init__(self):
		print("Outer Constructor")
		self.x = 10
	
	def dispData(self):
	
	print(self.x)
	class Inner:
		
		def __init__(self):
			print("Inner Constructor")
			self.y = 20

		def dispData(self):
			print(self.y)

outObj = Outer()
outObj.dispData()
