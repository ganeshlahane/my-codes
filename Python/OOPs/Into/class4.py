class cric:

	matchFormat = "T20"

	def __init__(self):
		self.name = "Dhoni"
		self.jerNo = 7

	#Decorator	
	def myclassmethod(fun):
		def inner (*args):
			print("in decorater")
			fun(args[0].__class__)
		return inner

	#instancemethod
	def disp(self):
		print("name = {} and jerNo = {}".format(self.name,self.jerNo))

	#myclassmethod
	@myclassmethod
	def dispformat(cls):
		print(cls)
		print(cls.matchFormat)

player1 = cric()
player1.disp()
player1.dispformat()