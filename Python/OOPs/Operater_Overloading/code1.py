class XYZ:

	def __init__(self,x,y):
		
		self.x = x
		self.y = y

	def __add__(self,obj):

		return self.x + obj.x , self.y + obj.y

	def __le__(self,obj):

		return self.x <= obj.x , self.y <= obj.y
	
	def __ge__(self,obj):

		return self.x >= obj.x , self.y >= obj.y

	def __gt__(self,obj):

		return self.x > obj.x , self.y > obj.y

	def __lt__(self,obj):

		return self.x < obj.x , self.y < obj.y

	def __eq__(self,obj):

		return self.x == obj.x , self.y == obj.y

obj1 = XYZ(10,20)
obj2 = XYZ(5,10)

output = obj1 + obj2
print(output)
print(obj1 <= obj2)
print(obj1 >= obj2)
print(obj1 > obj2)
print(obj1 < obj2)
print(obj1 == obj2)
#print(dir(object))