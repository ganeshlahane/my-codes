class abc:
	def __init__(self):
		self.x=10
		self._y=20
		self.__z=30
	def disp(self):
		print('In parent')
		print(self.x)
		print(self._y)
		print(self.__z)
class xyz(abc):
	def __init__(self):    
		super().__init__()
	def child(self):
		super().disp()
		print('In child')
		print(self.x)
		print(self._y)
		print(self._abc__z)
obj=xyz()
obj.child()
print(obj.x)
print(obj._y)
print(obj._abc__z)
