

'''
   1
   2 1
   3 2 1
   4 3 2 1

   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    num = x+1
    for y in range(x+1):
        print(num,end=" ")
        num = num-1
    print()
