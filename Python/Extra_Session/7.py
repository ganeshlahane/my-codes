
''' 
   1
   3 5
   5 7 9
   7 9 11 13
 
   '''

rows = int(input("Enter no. of rows: "))
num = 1
for x in range(rows):
    for y in range(x+1):
        print(num,end=" ")
        num = num+2
    print()
    num = num-(x*2)
