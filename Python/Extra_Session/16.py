

'''
         0
       0 2
     0 2 4
   0 2 4 6

   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end=" ")
    num = 0
    for z in range(x+1):
        print(num,end=" ")
        num = num+2
    print()

