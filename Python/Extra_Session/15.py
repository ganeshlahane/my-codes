

'''
             6
         12 15
      18 21 24
   24 27 30 33

   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    num = 6*(x+1)
    for y in range(rows-x-1):
        print(" ", end="\t")
    for z in range(x+1):
        print(num,end="\t")
        num = num+3
    print()

