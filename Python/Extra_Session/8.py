

'''
   1
   1 2
   1 2 1
   1 2 1 2
   
   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    for y in range(x+1):
        if(y%2==0):
            print("1",end=" ")
        elif(y%2==1):
            print("2",end=" ")
    print()
    
