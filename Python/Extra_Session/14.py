

'''
         *
       * *
     * * *
   * * * *

'''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end=" ")
    for z in range(x+1):
        print("*",end=" ")
    print()

