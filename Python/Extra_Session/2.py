
'''
   4
   4 3
   4 3 2
   4 3 2 1

   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    num = 4
    for y in range(x+1):
        print(num,end=" ")
        num = num-1
    print()
