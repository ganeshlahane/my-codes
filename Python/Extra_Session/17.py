

''' 
            5
         6 10  
      7 12 15    
   8 14 18 20

   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end="\t")
    num = 5+x
    for z in range(x+1):
        print(num*(z+1),end="\t")
        num = num - 1
    print()
