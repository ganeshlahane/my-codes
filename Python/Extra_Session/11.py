

'''
   5  7  9  11 
   10 12 14 
   15 17
   20

   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    num = 5*(x+1)
    for y in range(rows-x):
        print(num,end="\t")
        num = num+2
    print()
