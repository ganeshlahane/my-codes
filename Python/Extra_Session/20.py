

'''            a
             z z
           b b b
         y y y y
       c c c c c

       '''

rows = int(input("Enter no.of rows: "))
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end=" ")
    num1 = 97+x
    num2 = 122-x
    for z in range(x+1):
        if(x%2==0):
            print(chr(num1),end=" ")
            num1 = num1-1
        elif(x%2==1):
            print(chr(num2),end=" ")
            num2 =num2+1
    print()

