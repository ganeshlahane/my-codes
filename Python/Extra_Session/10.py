
'''
   *              * = 42
   # #            # = 35
   + + + +        + = 43
   $ $ $ $ $      $ = 36
   * * * * * *
     
   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    for y in range(x+1):
        if(x%4==0):
            print("*",end=" ")
        elif(x%4==1):
            print("#",end=" ")
        elif(x%4==2):
            print("+",end=" ")
        elif(x%4==3):
            print("$",end=" ")
    print()
