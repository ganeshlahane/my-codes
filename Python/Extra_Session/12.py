

'''
   16 9 4 1
   9 4 1
   4 1
   1

   '''

rows = int(input("Enter no. of rows: "))
for x in range(rows):
    num = rows-x
    for y in range(rows-x):
        print(num**2,end=" ")
        num = num-1
    print()
