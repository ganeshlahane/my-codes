

'''
           1
        4  1
     7  4  1
  10 7  4  1 

        '''

rows = int(input("Enter no. of rows:"))
for x in range(rows):
    for y in range(rows-x-1):
        print(" ",end="\t")
    num = 3*(x)+1
    for z in range(x+1):
        print(num,end="\t")
        num = num-3
    print()

