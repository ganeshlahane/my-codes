

"""
    Program4: write a program to check whether the number is divisible by 5 and 11.

"""

num1 = int(input("Enter number:"))

if (num1 % 5 == 0 and num1 % 11 == 0):

    print(num1,"is divisible by 5")

else:

    print(num1,"is not divisible by 5 and 11")
