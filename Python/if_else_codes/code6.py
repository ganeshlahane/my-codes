

"""
    program6: write a program to check whether the character is alphabet or not.
"""

var = input("Enter Character: ")

if var >= "A" and var <= "Z":
    print(var,"is alphabet")

elif var >= "a" and var <= "z":
    print(var,"is alphabet")

else:
    print(var,"is not alphabet")
