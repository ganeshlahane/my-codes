

# syntax of function:

def fun(x):                  # x = formal parameters
    print("In Fun")
    print(x)
fun(10)                      # 10 = arguments
fun("GaneshLahane")
fun(10.5)
