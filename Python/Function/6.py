

# Variable no. of argument:

# 1)

def sum(a,b):
    print("sum = ",(a+b))
sum(10,30)
sum(50,50)
sum(20,30,40)    # TypeError:sum() takes 2 arguments 3 were given

# 2)

def fun(a,*b):
    print(type(a))
    print(a)
    print(type(b))
    print(b)
fun(10)
fun(50,20)
fun(50,20,10)
fun(50,20,10,30)

# 3)

def addvar(*args):
    sum = 0
    for i in args:
        sum = sum+i
    print(sum)
x = int(input("Enter num1: "))
y = int(input("Enter num2: "))
z = int(input("Enter num3: "))
addvar(x,y,z)

# 4)

def div4and5(*a):
    for x in a:
        if(x%4==0 and x%5==0):
            print(a)

div4and5(20,40,60,30)

