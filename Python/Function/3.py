

# Passing array to function 

import numpy
def add(arr):
    sum = 0
    for i in range(len(arr)):
        sum = sum + arr[i]
    return(sum)
num1 = int(input("Enter size of array: "))
arr = numpy.zeros(num1,int)
for i in range(len(arr)):
    num2 = int(input("Enter number: "))
    arr[i] = num2
ans = add(arr)
print(ans)
