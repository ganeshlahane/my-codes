
'''
Types of parameters: 
                    '''
'''
# 1. positional parameter:

def fun(x,y):
    print(fun)
fun(10,20)       
fun()           # TypeError: 2 argumrnts required are  missing
fun(50)         # TypeError: 1 argument  required is missing


# 2. defualt parameter: 

def fun(x,y=20):
    print(x)
    print(y)
fun(10,30)
fun(30)
fun()          # TypeError: missing 1 required argument

'''

# 3.

def fun(p,x=10,y=20):
    print(p)
    print(x)
    print(y)
fun(10)
fun(30)
