

friends = list()

friends.append("Abhi")
friends.append("Sahil")
friends.append("Karan")
friends.append("Ganesh")
friends.append("Aatish")

lst1 = friends
print(lst1)

lst2 = friends.copy()
print(lst2)

print(id(lst1[0])) == (id(lst2[0]))
