'''
WAP to find the odd numbers from the array.
input: [1,2,3,4,5,6,7,8,9,10]
Output: [1,3,5,7,9]

'''
import array

iArr = array.array('i',[])
num = int(input("Enter no. of array ele: "))
for i in range (num):
    ele = int(input("Enter number: "))
    iArr.append(ele)
for i in range(len(iArr)):
    if(iArr[i]%2==1):
    	print(iArr[i],end=' ')
print()



