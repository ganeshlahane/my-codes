'''
Write a program to reverse a array
Input : [1,2,3,4,5]
Output : [5,4,3,2,1]
'''

import numpy

n = int(input("Enter array size: "))

arr = numpy.zeros(n,int)

for i in range(n):
	arr[i] = int(input("Enter elements in array: "))

l = len(arr)

for i in range(l//2):

	temp = arr[i]
	arr[i] = arr[l-i-1]
	arr[l-i-1] = temp

print(arr)
