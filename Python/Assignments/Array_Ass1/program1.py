
'''
 program1 : write a program to find even numbers from array.
 input : [1,2,3,4,6,8,10]
 Output : 2,4,6,8,10
 '''

import array

iArr = array.array('i',[])
num = int(input("Enter no. of array ele: "))
for i in range (num):
    ele = int(input("Enter number: "))
    iArr.append(ele)
for  i in range(len(iArr)):
    if(iArr[i]%2==0):
        print(iArr[i],end=' ')
print()
