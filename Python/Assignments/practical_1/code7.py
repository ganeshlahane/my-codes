

# write a program to check a character is an alphabet, a digit or a number.

char = int(:wqinput("Enter any character: "))

if(char>="A" and char<="Z") or (char>="a" and char<="z"):
    print(char,"is an alphabet")

elif(char>="48" and char<="57"):
    print(char,"is a number")

else:
    print(char,"is a special character")
