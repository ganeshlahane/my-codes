

# write a program to take input from user and find whether the number is divisible by 5 and 11.
# input: 55
# output: yes the given number is divisible by 5 and 11

num1 = int(input("Enter number: "))

if(num1 % 5==0 and num1 % 11==0):
    print("yes the given number is divisible by 5 and 11")
else:
    print("the given numbmer is not divisible by 5 and 11")
