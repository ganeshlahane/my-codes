

# write a program to find minimum between 3 numbers.
# input:2 4 3
# output:2

num1 = int(input("Enter Number: "))
num2 = int(input("Enter Number: "))
num3 = int(input("Enter Number: "))

mini = 0

if num1<num2 and num1<num3:
    mini = num1
if num2<num1 and num2<num3:
    mini = num2
if num3<num1 and num3<num2:
    mini = num3

print(mini,"is the minimum number")

