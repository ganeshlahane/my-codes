

# write a program to check the input is the leap year or not.
# input: 2000
# output: leap year

# input: 1999
# output: is not a leap year

num1 = int(input("Enter year: "))

if (num1 % 4==0):
    print(num1,"is a leap year")

else:
    print(num1,"is not a leap year")
