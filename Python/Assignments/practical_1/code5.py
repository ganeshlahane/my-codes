

# write a program to check whether the alphabet lies between I to S.

# input: j
# output: yes

# input: t
# output: no

# input: K
# input: yes

char = input("Enter any alphabet: ")

if (char>="A" and char<="Z") or (char>="a" and char<="z"):
    if(char>="I" and char<="S") or (char>="i" and char<="s"):
        print("yes")
    else:
        print("no")
else:
    print("Invalid input")
