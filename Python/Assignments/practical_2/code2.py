

# Statement: 2    A b C
#                 d E f
#                 G h I



rows = int(input("Enter no. of rows: "))
num = 65
for x in range(rows):
    for y in range(rows):
        data = chr(num)
        if((x+y) % 2 == 0):
            print(data,end=" ")
        else:
            print(chr(num+32),end=" ")
        num = num + 1
    print()
