

# Statement:4  d c b a
#              c b a
#              b a
#              a

rows = int(input("Enter no. of rows: "))


for x in range(rows):
    num = 96+rows-x
    for y in range(rows-x):
        data = chr(num)
        print(data,end=" ")
        num = num-1
    print()
