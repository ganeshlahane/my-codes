

num = int(input("Enter number:"))
sum = 0

if (num == 1):
    for x in range(1,10):
        sum = sum + x
    print(sum)

elif (num == 2):
    for x in range(10,100):
        sum = sum + x
    print(sum)

elif (num == 3):
    for x in range(100,1000):
        sum = sum + x
    print(sum)

elif (num == 4):
    for x in range(1000,10000):
        sum = sum + x
    print(sum)

else: 
    print("Invalid Input")
