

# Statement: * 
#            * *
#            * * *
#            * * * * 


rows = 4

for x in range(rows):
    for y in range(x + 1):
        print("*", end=" ")
    print()
