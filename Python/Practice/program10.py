'''
	1  	2  9   4
	25  6  49  8
	81  10 121 12
	169 14 225 16
	              '''

rows = int(input("Enter No. of rows: "))
num = 1
for i in range(rows):
	for j in range(rows):
		print(num*num,end="\t")
		num += 1
	print()

