# WAP to print count of odd numbers which are divisible by 9

num1 = int(input("Enter Start Number: "))
num2 = int(input("Enter End Number: "))

count = 0

for x in range(num1,num2+1):
	if(x%2==1 and x%9==0):
		count = count+1
		print(x,end=" ")
		print()
print("No. of odd numbers: ",count)	