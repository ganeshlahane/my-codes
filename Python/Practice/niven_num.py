# Niven Number OR Harshad Number

num1 = int(input("Enter Start Number: "))
num2 = int(input("Enter End Number: "))

for i in range(num1,num2+1):

	add = 0
	temp = i

	while (temp>0):
		rem = temp%10
		add = add+rem
		temp = temp//10

	if (i % add == 0):
		print(i,"is a Niven Number")

	else:
		print(i,"is not a Niven Number")