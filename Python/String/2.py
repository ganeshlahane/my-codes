
'''
 Indexing in String:
                       
                      |C|o|r|e|2|W|e|b| |T|e|c|h|n|o|l|o|g|i|e|s|

                      '''

str1 = "Core2Web Technologies"
print(str1)
print(str1[4])
print(str1[15])

#slicing:

print(str1[1::])
print(str1[1:15:2])
print(str1[7:-5:2])
print(str1[-17:-9:])
print(str1[-3:-25:-1])
print(str1[3:-17:4])
print(str1[-3:-17:3])
